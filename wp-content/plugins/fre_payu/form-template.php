<?php
    $payu_info = ae_get_option('payu');

    if( isset( $payu_info['service_provider'] ) && !empty( $payu_info['service_provider'] ) ) {
        $service_provider = $payu_info['service_provider'];
    } else {
        // Default value
        $service_provider = 'ae_payment';
    }
?>
<div id="fre-payment-payu" class="panel-collapse collapse fre-payment-proccess">
    <form class="modal-form" id="payu_form" action="#" method="POST" autocomplete="on">
        <div class="fre-input-field">
            <label class="fre-field-title" for=""><?php _e('First Name',ET_DOMAIN);?></label>
            <input  tabindex="20" id="payu_firstname" name="payu_firstname" type="text" size="20"   class="bg-default-input not_empty required" placeholder="<?php _e('Your first name',ET_DOMAIN);?>" />
        </div>
        <div class="fre-input-field">
            <label class="fre-field-title" for=""><?php _e('Last Name',ET_DOMAIN);?></label>
            <input  tabindex="20" id="payu_lastname" name="payu_lastname" type="text" size="20"  class="bg-default-input not_empty" placeholder="<?php _e('Your last name',ET_DOMAIN);?>" />
        </div>
        <div class="fre-input-field">
            <label class="fre-field-title" for=""><?php _e('Email',ET_DOMAIN);?></label>
            <input  tabindex="20" id="payu_email" type="email" name="payu_email" size="20"   class="bg-default-input not_empty" placeholder="e.g exemple@enginethemes.com" />
        </div>
        <div class="fre-input-field">
            <label class="fre-field-title" for=""><?php _e('Phone',ET_DOMAIN);?></label>
            <input  tabindex="20" id="payu_phone" type="text" size="20" class="bg-default-input not_empty" placeholder="0123456789" />
        </div>
        <div class="fre-proccess-payment-btn">
            <button class="fre-btn" id="button_payu" ><?php _e('Make Payment',ET_DOMAIN);?></button>
        </div>
    </form>
    <div style="display: none; height: 0; width:0;">
        <form method="post" action="#" id="payu_hidden_form">
            <input type="hidden" name="key" id="payu_key"/>
            <input type="hidden" name="hash" id="payu_hash" />
            <input type="hidden" name="txnid" id="payu_txnid"/>
            <input type="hidden" name="firstname" id="payu_firstname_h"/>
            <input type="hidden" name="productinfo" id="payu_productinfo"/>
            <input type="hidden" name="email" id="payu_email_h"/>
            <input type="hidden" name="phone" id="payu_phone_h"/>
            <input type="hidden" name="amount" id="payu_amount"/>
            <input type="hidden" name="surl" id="payu_surl"/>
            <input type="hidden" name="furl" id="payu_furl"/>
            <input type="hidden" name="curl" id="payu_curl"/>
            <input type="hidden" name="service_provider" value="<?php echo $service_provider; ?>"/>
            <button type="submit" class="btn  btn-primary" id="button_payu_h" >Submit </button>
    </form>
    </div>
</div>