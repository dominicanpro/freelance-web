(function($, Views) {
    Views.PayuForm = Views.Modal_Box.extend({
        el: $('div#fre-payment-payu'),
        events: {
            'submit form#payu_form': 'submitPayu'
        },
        initialize: function(options) {
            Views.Modal_Box.prototype.initialize.apply(this, arguments);
            // bind event to modal
            _.bindAll(this, 'setupData');
            this.blockUi = new Views.BlockUi();
            // catch event select extend gateway
            AE.pubsub.on('ae:submitPost:extendGateway', this.setupData);
            this.initValidate();
        },
        // callback when user select Paymill, set data and open modal
        setupData: function(data) {
            if (data.paymentType == 'payu') {
                // this.openModal();
                this.data = data,
                plans = JSON.parse($('#package_plans').html());
                var packages = [];
                _.each(plans, function(element) {
                    if (element.sku == data.packageID) {
                        packages = element;
                    }
                })
              var align = parseInt(ae_payu.currency.align);
                if (align) {
                    var price = ae_payu.currency.icon + packages.et_price;
                } else {
                    var price = packages.et_price + ae_payu.currency.icon;
                }

                this.data.price = packages.et_price;

                //if($('.coupon-price').html() !== '' && $('#coupon_code').val() != '' ) price  =   $('.coupon-price').html();
                this.$el.find('span.plan_name').html(packages.post_title + ' (' + price + ')');
                this.$el.find('span.plan_desc').html(packages.post_content);
            }
        },
        //setup validate form
        initValidate:function(){
            if(typeof this.validate_ae_payu === "undefined"){
                this.validate_ae_payu = this.$('form#payu_form').validate({
                    rules: {
                        payu_firstname: {
                            required: true,
                        },
                        payu_email: {
                            required: true,
                            email: true
                        },
                    },
                    validClass: "valid", // the classname for a valid element container
                    errorClass: "message", // the classname for the error message for any invalid element
                    errorElement: 'div', // the tagname for the error message append to an invalid element container
                    highlight: function(element, errorClass, validClass) {
                        var required_id = $(element).attr('id');
                        var $container = $(element).closest('div');
                        if (!$container.hasClass('error')) {
                            $container.addClass('error').removeClass(validClass);
                        }
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        var $container = $(element).closest('div.fre-input-field');
                        if ($container.hasClass('error')) {
                            $container.removeClass('error').addClass(validClass);
                        }
                        $container.find('div.message').remove().end().find('i.fa-exclamation-triangle').remove();
                    }
                })    
            }
        },
        
        // catch user event click on pay
        submitPayu: function(event) {
            event.preventDefault();
            if(this.validate_ae_payu.form()){
                var $form = $(event.currentTarget),
                    $container = $form.parents('.step-wrapper'),
                    data = this.data;
                    data.payu_firstname = $form.find('#payu_firstname').val();
                    data.payu_lastname  = $form.find('#payu_lastname').val();
                    data.payu_email     = $form.find('#payu_email').val();
                    data.payu_phone     = $form.find('#payu_phone').val();
                    if(data.payu_firstname=="" && data.payu_emai==""){
                        alert("error");
                        return false;
                    }
                 var view = this;   
                    //neu muon post sang setup payment
                $.ajax({
                    type : 'post',
                    url : ae_globals.ajaxURL,
                    data : data,
                     beforeSend: function() {
                        view.blockUi.block($container);
                    },
                    success:function(res){
                       // view.blockUi.unblock();
                        if(res.success){
                            $('#payu_hash').val(res.data.data_arr.hash);
                            $('#payu_txnid').val(res.data.data_arr.txnid);
                            $('#payu_key').val(res.data.data_arr.key);
                            $("#payu_amount").val(res.data.data_arr.amount);
                            $("#payu_firstname_h").val(res.data.data_arr.firstname);
                            $("#payu_email_h").val(res.data.data_arr.email);
                            $("#payu_phone_h").val(res.data.data_arr.phone);
                            $("#payu_productinfo").val(res.data.data_arr.productinfo);
                            $("#payu_hidden_form").attr("action", res.data.url);
                            $('#payu_surl').val(res.data.surl);
                            $('#payu_furl').val(res.data.furl);
                            $('#payu_curl').val(res.data.furl);
                            $('#button_payu_h').trigger("click");
                        }else{
                            AE.pubsub.trigger('ae:notification', {
                                msg: ae_payu.errorpost,
                                notice_type: 'error'
                            });
                            return false
                        }
                    }
                    
                });
            }
        },
    });
    // init Payu form
    $(document).ready(function() {
        new Views.PayuForm();
    });
})(jQuery, AE.Views);


