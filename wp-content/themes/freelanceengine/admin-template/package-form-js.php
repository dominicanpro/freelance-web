<script type="text/template" id="template_edit_form">
	<form action="qa-update-badge" class="edit-plan engine-payment-form">
		<input type="hidden" name="id" value="{{= id }}">

		<div class="form payment-plan">
			<div class="form-item f-left-all clearfix">
				<div class="width33p">
					<div class="label"><?php _e("SKU", ET_DOMAIN); ?><span class="dashicons dashicons-editor-help" title="SKU debe ser único y necesita estar compuesto por letras o números."></span></div>
					<input value="{{= sku }}" class="bg-grey-input width50p not-empty required" name="sku" type="text" />
				</div>
			</div>
			<div class="form-item">
				<div class="label"><?php _e("Nombre del paquete", ET_DOMAIN); ?></div>
				<input value="{{= post_title }}" class="bg-grey-input not-empty required" name="post_title" type="text">
			</div>
			<div class="form-item f-left-all clearfix">
				<div class="width33p">
					<div class="label"><?php _e("Precio", ET_DOMAIN); ?></div>
					<input value="{{= et_price }}" class="bg-grey-input width50p not-empty is-number required number" name="et_price" type="text" />
					<?php ae_currency_code(); ?>
				</div>
				<div class="width33p">
					<div class="label"><?php _e("Disponibilidad",ET_DOMAIN);?></div>
					<input value="{{= et_duration }}" class="bg-grey-input width50p not-empty is-number required number" type="text" name="et_duration" />
					<?php _e("days",ET_DOMAIN);?>
				</div>
				<div class="width33p">
					<div class="label"><?php _e("Cantidad de proyectos que puede publicar", ET_DOMAIN); ?></div>
					<input value="{{= et_number_posts }}" class=" bg-grey-input width50p not-empty is-number required" type="text" name="et_number_posts" />
				</div>

			</div>
			<div class="form-item">
			<div class="label"><?php _e("Tipo de proyecto",ET_DOMAIN);?></div>
				<?php ae_tax_dropdown( 'project_type' ,
										array(  'class' => 'chosen-single tax-item',
												'hide_empty' => false,
												'hierarchical' => true ,
												'id' => 'project_type' ,
												'show_option_all' => __("Seleccione el tipo de proyecto", ET_DOMAIN) ,
												'value' => 'id',
												'name' => 'project_type'
											)
										) ;?>
			</div>
			<div class="form-item">
				<div class="label"><?php _e("Descripción corta de su paquete",ET_DOMAIN);?></div>
				<input class="bg-grey-input not-empty" name="post_content" type="text" value="{{= post_content }}" />
			</div>
			<!--
			<div class="form-item">
				<div class="label"><?php _e("Cualidades del proyecto",ET_DOMAIN);?></div>
				<input type="checkbox" name="et_featured" value="1" <# if (typeof et_featured !== 'undefined' && et_featured == 1 ) { #> checked="checked" <# } #> 	/>
				<?php _e("Este plan permitirá",ET_DOMAIN);?>
			</div>
			-->
			<div class="submit">
				<button  class="btn-button engine-submit-btn add_payment_plan">
					<span><?php _e( 'Guardar plan' , ET_DOMAIN ); ?></span><span class="icon" data-icon="+"></span>
				</button>
				or <a href="#" class="cancel-edit"><?php _e( "Cancelar" , ET_DOMAIN ); ?></a>
			</div>
		</div>
	</form>
</script>
