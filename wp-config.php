<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'i6176733_wp1' );

/** MySQL database username */
define( 'DB_USER', 'i6176733_wp1' );

/** MySQL database password */
define( 'DB_PASSWORD', 'F.WmadF06uyfvpRh5zQ58' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7FaHVukKTQftxTdgQAuCy61GNZvH7BLyXVqxee4tl1hvBDiYKRpr0WhmO4fCJU6m');
define('SECURE_AUTH_KEY',  'IenU8CHVdJuakCryzUZdo73zPXoDWQ4WRivJLSIp4RckCl8FPE9gfAU7T8kI0psK');
define('LOGGED_IN_KEY',    'my3qIorNJIgeOzp1aCvg91wwvZ4Y52d2jQtss4zuIhHWaCHHyZGF5CcF5gqcg9qs');
define('NONCE_KEY',        'dDGv2sUwQl6vTURaF4Wa4732yYWR2dEseT2eXddr197lkDYwz5Jz6MXE8S1pPrFv');
define('AUTH_SALT',        '31pkWcp6RyQK6bN87ielMCihLOxivaHErhZdX4G1HN81iF3BQ8FlPxTgw0UiIZKe');
define('SECURE_AUTH_SALT', 'u66VbI2dwK0S80ljm3fQCDdkdl0Fef2F23qjrjfz19yx2bJplFGADKnthHNeQpZd');
define('LOGGED_IN_SALT',   '1jwOKyqzjj9K8Z2ddlNtCZlc6HgeBSRrapnMfyycGdtCZJ3EPGN1eQvvYlKslk5R');
define('NONCE_SALT',       '1tU592pMn2OzE5VTRyDPuj9LvpJs7qmUHCtrDjNglpedbO88HjaydAwP7dVYoJpr');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');
define('FS_CHMOD_DIR',0755);
define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed externally by Installatron.
 * If you remove this define() to re-enable WordPress's automatic background updating
 * then it's advised to disable auto-updating in Installatron.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );