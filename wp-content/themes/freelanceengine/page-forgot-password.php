<?php
get_header();
?>
<div class="fre-page-wrapper">
	<div class="fre-page-section">
		<div class="container">
			<div class="fre-authen-wrapper">
				<div class="fre-authen-lost-pass">
					<h2><?php _e('Reinicia tu contraseña', ET_DOMAIN);?></h2>
					<p><?php _e("Coloca tu correo en el cuadro de abajo. Identificaremos tu cuenta y te enviaremos un correo de reinicio de sesión.", ET_DOMAIN);?></p>
					<form role="form" id="forgot_form" class="auth-form forgot_form">
						<!-- <ul class="fre-validate-error">
							<li>Email exists</li>
						</ul> -->
						<div class="fre-input-field">
							<input type="text" id="user_email" name="user_email" placeholder="<?php _e('Tu dirección de E-mail', ET_DOMAIN);?>">
							<!-- <div class="message">This field is required.</div> -->
						</div>
						<div class="fre-input-field">
							<button class="fre-submit-btn btn-submit  primary-bg-color"><?php _e('Enviar solicitud de reinicio', ET_DOMAIN);?></button>
						</div>
					</form>
					<div class="fre-authen-footer">
						<p><?php _e('¿Ya tienes una cuenta?', ET_DOMAIN);?> <a href="<?php echo et_get_page_link("login") ?>"><?php _e('Ingresar', ET_DOMAIN);?></a></p>		
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
