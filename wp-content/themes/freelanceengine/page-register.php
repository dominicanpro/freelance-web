<?php
/**
 * Template Name: Register Page Template
*/
global $post;
	get_header();
	if(!isset($_REQUEST['role'])){
?>
<div class="fre-page-wrapper">
	<div class="fre-page-section">
		<div class="container">
			<div class="fre-authen-wrapper">
				<div class="fre-register-default">
					<h2><?php _e('Registrar una cuenta GRATUITA', ET_DOMAIN)?></h2>
					<div class="fre-register-wrap">
						<div class="row">
							<div class="col-sm-6">
								<div class="register-employer">
									<h3><?php _e('Contratante', ET_DOMAIN);?></h3>
									<p><?php _e('Publica proyectos, contrata freelancers para ellos.', ET_DOMAIN);?></p>
									<a class="fre-small-btn primary-bg-color" href="<?php echo  et_get_page_link( 'register',array('role' => EMPLOYER) );?>"><?php _e('Registrarse', ET_DOMAIN);?></a>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="register-freelancer">
									<h3><?php _e('Freelancer', ET_DOMAIN);?></h3>
									<p><?php _e('Crea un perfil profesional y haz ofertas en proyectos.', ET_DOMAIN);?></p>
									<a class="fre-small-btn primary-bg-color" href="<?php echo  et_get_page_link( 'register',array('role' => FREELANCER) );?>"><?php _e('Registrarse', ET_DOMAIN);?></a>
								</div>
							</div>
						</div>
					</div>
					<div class="fre-authen-footer">
						<?php
			                if(fre_check_register() && function_exists('ae_render_social_button')){
			                    $before_string = __("Puedes utilizar tus redes sociales para registrarte", ET_DOMAIN);
			                    ae_render_social_button( array(), array(), $before_string );
			                }
			            ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	}else{
		$role = $_REQUEST['role'];
		$re_url = '';
		if( isset($_GET['ae_redirect_url']) ){
			$re_url = $_GET['ae_redirect_url'];
		}
?>
	<div class="fre-page-wrapper">
		<div class="fre-page-section">
			<div class="container">
				<div class="fre-authen-wrapper">
					<div class="fre-authen-register">
						<?php if($role == 'employer'){ ?>
								<h2><?php _e('Registrar cuenta empresarial', ET_DOMAIN);?></h2>
						<?php }else{ ?>
								<h2><?php _e('Registrar cuenta de Freelancer', ET_DOMAIN);?></h2>
						<?php } ?>
						<form role="form" id="signup_form">
							<input type="hidden" name="ae_redirect_url"  value="<?php echo $re_url ?>" />
							<input type="hidden" name="role" id="role" value="<?php echo $role;?>" />
							<div class="fre-input-field">
								<input type="text" name="first_name" id="first_name" placeholder="<?php _e('Nombre', ET_DOMAIN);?>">
							</div>
							<div class="fre-input-field">
								<input type="text" name="last_name" id="last_name" placeholder="<?php _e('Apellido', ET_DOMAIN);?>">
							</div>
							<div class="fre-input-field">
								<input type="text" name="user_email" id="user_email" placeholder="<?php _e('Email', ET_DOMAIN);?>">
							</div>
							<div class="fre-input-field">
								<input type="text" name="user_login" id="user_login" placeholder="<?php _e('Usuario', ET_DOMAIN);?>">
							</div>
							<div class="fre-input-field">
								<input type="password" name="user_pass" id="user_pass" placeholder="<?php _e('Contraseña', ET_DOMAIN);?>">
							</div>
							<div class="fre-input-field">
								<input type="password" name="repeat_pass" id="repeat_pass" placeholder="<?php _e('confirmar contraseña', ET_DOMAIN);?>">
							</div>
							<?php ae_gg_recaptcha( $container = 'fre-input-field' );?>
							<div class="fre-input-field">
								<button class="fre-btn btn-submit primary-bg-color"><?php _e('Registrarse', ET_DOMAIN);?></button>
							</div>
						</form>
						<?php
							$tos = et_get_page_link('tos', array() ,false);
			                $url_tos = '<a href="'.et_get_page_link('tos').'" class="secondary-color" rel="noopener noreferrer" target="_Blank">'.__('Términos de usuario y políticas deprivacidad', ET_DOMAIN).'</a>';
			                if($tos) {
			                	echo "<p>";
			                	printf(__('Al registrarme, acepto los términos y condiciones %s', ET_DOMAIN), $url_tos );
			                	echo "</p>";
			                }
						?>
						<div class="fre-authen-footer">
							<p><?php _e('¿Tienes una cuenta?', ET_DOMAIN);?> <a href="<?php echo et_get_page_link("login") ?>" class="secondary-color"><?php _e('Ingresar', ET_DOMAIN);?></a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
	}
	get_footer();
?>
