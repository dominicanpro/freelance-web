<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Freelance Engine
 * @since Freelance Engine 1.0
 */

get_header(); ?>

<section class="blog-header-container">
	<div class="container">
		<!-- blog header -->
		<div class="row">
		    <div class="col-md-12 blog-classic-top">
		        <h2><?php _e("No encontrado", ET_DOMAIN); ?></h2>
		    </div>
		</div>
		<!--// blog header  -->
	</div>
</section>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block; text-align:center;"
     data-ad-layout="in-article"
     data-ad-format="fluid"
     data-ad-client="ca-pub-2209019656458371"
     data-ad-slot="5930082441"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
<div class="container">
	<div class="page-notfound-content">
		<h2><?php _e("Error 404", ET_DOMAIN); ?></h2>
        <h4><?php _e("Lo sentimos, la página que buscas no existe", ET_DOMAIN ); ?></h4>
        <p>
        	<?php printf(__('Regresar a <a href="%s">inicio </a> también de vuelta  return a <a href="#" onclick="window.history.back()">la página anterior</a> page.', ET_DOMAIN),
        			home_url()); ?></p>
    </div>
</div>
<?php
get_footer();
