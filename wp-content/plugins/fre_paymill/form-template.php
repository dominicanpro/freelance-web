<?php 
	$options = AE_Options::get_instance();
    // save this setting to theme options
    $website_logo = $options->site_logo;
?>
<div id="fre-payment-paymill" class="panel-collapse collapse fre-payment-proccess">
	<form class="modal-form" id="paymill_form" action="#" method="POST" novalidate="novalidate" autocomplete="on">
		<div class="fre-input-field">
            <label class="fre-field-title" for=""><?php _e('Name on card',ET_DOMAIN);?></label>
            <input tabindex="23" name="paymill_card_holdername" id="paymill_card_holdername"  data-paymill="paymill_card_holdername" class=" bg-default-input not_empty" type="text" />
        </div>
        <div class="row card-number-wrap">
            <div class="col-lg-8 col-md-7 col-sm-7">
                <div class="fre-input-field">
                    <label class="fre-field-title" for=""><?php _e('Card number',ET_DOMAIN);?></label>
                    <input  tabindex="20" id="paymill_number" type="text" size="20"  data-paymill="number" class="card-number bg-default-input not_empty" placeholder="&#8226;&#8226;&#8226;&#8226; &nbsp; &#8226;&#8226;&#8226;&#8226; &nbsp; &#8226;&#8226;&#8226;&#8226; &nbsp; &#8226;&#8226;&#8226;&#8226;" />
                </div>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-5">
                <div class="fre-input-field fre-card-expiry">
                    <label class="fre-field-title " for=""><?php _e('Expiry date',ET_DOMAIN);?></label>
                    <input tabindex="22" type="text" size="4" data-paymill="exp-year" placeholder="YYYY"  class="fre-card-expiry-month bg-default-input not_empty" id="paymill_exp_year"/>
                    <input tabindex="21" type="text" size="2" data-paymill="exp-month" placeholder="MM"  class="fre-card-expiry-year bg-default-input not_empty" id="paymill_exp_month"/>
                </div>
            </div>
        </div>
        <div class="fre-input-field">
            <label class="fre-field-title" for=""><?php _e('Card code',ET_DOMAIN);?></label>
            <input tabindex="24" type="text" size="3"  data-paymill="cvc" class="card-cvc bg-default-input not_empty input-cvc " placeholder="CVC" id="paymill_cvc" />
        </div>
        <div class="fre-proccess-payment-btn">
            <button class="fre-btn" id="submit_paymill" ><?php _e('Make Payment',ET_DOMAIN);?></button>
        </div>
	</form>
</div>