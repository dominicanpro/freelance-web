<?php
define( 'ADMIN_PATH', TEMPLATEPATH . '/admin' );

if ( ! class_exists( 'AE_Base' ) ) {
	return;
}

/**
 * Handle admin features
 * Adding admin menus
 */
class ET_Admin extends AE_Base {
	function __construct() {

		/**
		 * admin setup
		 */
		$this->add_action( 'init', 'admin_setup' );

		/**
		 * update first options
		 */
		$this->add_action( 'after_switch_theme', 'update_first_time' );

		//declare ajax classes
		new AE_CategoryAjax( new AE_Category( array(
			'taxonomy' => 'project_category'
		) ) );
		new AE_CategoryAjax( new AE_Category( array(
			'taxonomy' => 'project_type'
		) ) );

		$this->add_ajax( 'ae-reset-option', 'reset_option' );

		/* User Actions */
		$this->add_action( 'ae_upload_image', 'ae_upload_image', 10, 2 );

		/**
		 * set default options
		 */
		$options = AE_Options::get_instance();
		if ( ! $options->init ) {
			$options->reset( $this->get_default_options() );
		}

		// kick subscriber user
		if ( ! current_user_can( 'manage_options' ) && basename( $_SERVER['SCRIPT_FILENAME'] ) != 'admin-ajax.php' ) {

			// wp_redirect( home_url(  ) );
			// exit;

		}
		$this->add_filter( 'ae_setup_wizard_template', 'fre_setup_wizard_template' );
		$this->add_filter( 'notice_after_installing_theme', 'fre_notice_after_installing_theme' );
		$this->add_action( 'ae_insert_sample_data_success', 'fre_after_insert_sample_data' );
	}

	/**
	 * update user avatar
	 */
	public function ae_upload_image( $attach_data, $data ) {

		if ( isset( $data["method"] ) && $data["method"] == "change_avatar" ) {
			if ( ! isset( $data['author'] ) ) {
				return;
			}

			$ae_users = AE_Users::get_instance();

			//update user avatar
			$user = $ae_users->update( array(
				'ID'            => $data['author'],
				'et_avatar'     => $attach_data['attach_id'],
				'et_avatar_url' => $attach_data['thumbnail'][0]
			) );
		}
		switch ( $data ) {
			case 'site_logo_black':
			case 'site_logo_white':
				$options = AE_Options::get_instance();

				// save this setting to theme options
				$options->$data = $attach_data;
				if ( $data == 'site_logo_black' ) {
					$options->site_logo = $attach_data;
				}
				$options->save();

				break;

			default:
				// code...
				break;
		}
	}

	/**
	 * ajax function reset option
	 */
	function reset_option() {

		$option_name     = $_REQUEST['option_name'];
		$default_options = $this->get_default_options();

		if ( isset( $default_options[ $option_name ] ) ) {
			$options               = AE_Options::get_instance();
			$options->$option_name = $default_options[ $option_name ];
			$options->save();
			wp_send_json( array(
				'msg' => $default_options[ $option_name ]
			) );
		}
	}

	function get_template_default_options( $name ) {
		$default_options = $this->get_default_options();
		if ( isset( $default_options[ $name ] ) ) {
			return $default_options[ $name ];
		}
	}

	function admin_custom_css() {
		?>
        <style type="text/css">
            .custom-icon {
                margin: 10px;
            }

            .custom-icon input {
                width: 80%;
            }
        </style>
		<?php
	}

	/**
	 * retrieve site default options
	 */
	function get_default_options() {

		return apply_filters( 'fre_default_setting_option', array(
			'blogname'        => get_option( 'blogname' ),
			'blogdescription' => get_option( 'blogdescription' ),
			'copyright'       => '<span class="enginethemes"> <a href=http://www.enginethemes.com/themes/freelanceengine/ >FreelanceEngine</a> - Powered by WordPress </span>',

			'project_demonstration'         => array(
				'home_page'    => 'The best way to <br/>  find a professional',
				'list_project' => 'A Million of Project.<br/> Find it out!'
			),
			'profile_demonstration'         => array(
				'home_page'    => 'Need a job? <br/> Tell us your story',
				'list_profile' => 'Need a job? <br/> Tell us your story'
			),

			// default forgot passmail
			'forgotpass_mail_template'      => '<p>Hello [display_name],</p><p>You have just sent a request to recover the password associated with your account in [blogname]. If you did not make this request, please ignore this email; otherwise, click the link below to create your new password:</p><p>[activate_url]</p><p>Regards,<br />[blogname]</p>',

			// default register mail template
			'register_mail_template'        => '<p>Hello [display_name],</p><p>You have successfully registered an account with &nbsp;&nbsp;[blogname].&nbsp;Here is your account information:</p><ol><li>Username: [user_login]</li><li>Email: [user_email]</li></ol><p>Thank you and welcome to [blogname].</p>',

			// default register social mail template
			'register_social_mail_template' => '<p>Hello [display_name],</p><p>You have successfully registered an account with &nbsp;&nbsp;[blogname].&nbsp;Here is your account information:</p><ol><li>Username: [user_login]</li><li>Email: [user_email]</li><li>Change password: [active_link]</li></ol><p>Thank you and welcome to [blogname].</p>',


			// default confirm mail template
			'password_mail_template'        => '<p>Hello [display_name],</p><p>You have successfully registered an account with &nbsp;&nbsp;[blogname].&nbsp;Here is your account information:</p><ol><li>Username: [user_login]</li><li>Email: [user_email]</li><li>Password: [password]</li></ol><p>Thank you and welcome to [blogname].</p>',

			//  default reset pass mail template
			'resetpass_mail_template'       => "<p>Hello [display_name],</p><p>You have successfully changed your password. Click this link &nbsp;[site_url] to login to your [blogname]'s account.</p><p>Sincerely,<br />[blogname]</p>",

			// default confirm mail template
			'confirm_mail_template'         => '<p>Hello [display_name],</p><p>You have successfully registered an account with &nbsp;&nbsp;[blogname].&nbsp;Here is your account information:</p><ol><li>Username: [user_login]</li><li>Email: [user_email]</li></ol><p>Please click the link below to confirm your email address.</p><p>[confirm_link]</p><p>Thank you and welcome to [blogname].</p>',

			// default confirmed mail template
			'confirmed_mail_template'       => "<p>Hi [display_name],</p><p>Your email address has been successfully confirmed.</p><p>Thank you and welcome to [blogname].</p>",

			//  default inbox mail template
			'inbox_mail_template'           => "<p>Hello [display_name],</p><p>You have just received the following message from user: <a href=\"[sender_link]\">[sender]</a></p>
                                        <p>|--------------------------------------------------------------------------------------------------|</p>
                                        [message]
                                        <p>|--------------------------------------------------------------------------------------------------|</p>
                                        <p>You can answer the user by replying this email.</p><p>Sincerely,<br />[blogname]</p>",

			//  default inbox mail template
			'publish_mail_template'         => "<p>Hello [display_name],</p>
                                        <p>Your listing: [title] in [blogname] is publish.</p>
                                        <p>You can follow this link: [link] to view your listing offer.</p>
                                        <p>Sincerely,<br />[blogname]</p>",

			'archive_mail_template' => "<p>Hello [display_name],</p>
                                        <p>Your listing: [title] in [blogname] has been archived due to expiration or manual administrative action.</p>
                                        <p>If you want to continue displaying this listing in our website, please go to your dashboard at [dashboard] to renew your listing offer.</p>
                                        <p>Sincerely,<br />[blogname]</p>",

			'reject_mail_template'                      => "<p>Hello [display_name],</p>
                                        <p>Your listing: [link] in [blogname] has been rejected due to expiration or manual administrative action.</p>
                                        <p>Reasons reject: [reject_message].</p>
                                        <p>Please contact the administrators via [admin_email] for more information, or go to your dashboard at [dashboard] to edit your listing offer and post it again.</p>
                                        <p>Sincerely,<br />[blogname]</p>",
			'invite_mail_template'                      => "<p>Hello [display_name],</p>
                                        <p>You have a invitation  from  [blogname] to joint a project.</p>
                                        <p>You can view these project at link : [link]</p>
                                        <p>Sincerely,<br />[blogname]</p>",
			'bid_mail_template'                         => "<p>Hello [display_name],</p>
                                    <p>You have a new bid on the project : [title].</p>
                                    <p>Here is the freelancer's message : [message].</p>
                                    <p>You can have more details in : [link]</p>
                                    <p>Sincerely,</p>
                                    <p>[blogname]</p>",
			'complete_mail_template'                    => '<p>Hi,</p>
                                        <p>Your working project <b>[title]</b> has been marked as "Completed" by employer [employer].</p>
                                        <p>Now, you can review the project and rate for her/him in:</p>
                                        <p>[link_review]</p>
                                        <p>Regards,<br />[blogname]</p>',
			'bid_accepted_template'                     => "<p>Hello [display_name],</p>
                                        <p>Your bid on the project [link] has been accepted.</p>
                                        <p>Enter the Workspace [workspace] for further discussion with the employer.</p>
                                        <p>Sincerely,</p>
                                        <p>[blogname]</p>",
			'bid_accepted_alternative_template'         => "<p>Hi,</p>
                                            <p>Employer [employer] started working on his project [link] with an alternative bidder.</p>
                                            <p>Thank you for your bid on this project.</p>
                                            <p>Regards,</p>
                                            <p>[blogname]</p>",
			'new_message_mail_template'                 => "<p>Hello [display_name],</p>
                                            <p>You have a new message on project [title]. Here is the message details:</p>
                                            <p>[message]</p>
                                            <p>You can view all message in [workspace]</p>
                                            <p>Sincerely,<br>[blogname]</p>",
			'new_payment_mail_template'                 => "<p>Hi Admin,</p>
                                            <p>User [user_name] has bought the [package_name] package on your site. Please review and confirm (if required) this payment.</p>
                                            <p>Regards,<br>[blogname]</p>",
			'cash_notification_mail'                    => "<p>Dear [display_name],</p>
                                        <p>[cash_message]</p>
                                        <p>Sincerely, <br/>[blogname].</p>",
			'ae_receipt_mail'                           => '<p>Dear [display_name],</p>
                                    <p>
                                        Thank you for your payment.<br />
                                        Here are the details of your transaction:<br />
                                        <strong>Detail</strong>: Purchase the [package_name] package. This package contains [number_of_bids] project bids.
                                    </p>
                                    <p>
                                        <strong> Customer info</strong>:<br />
                                        [display_name] <br />
                                        Email: [user_email]. <br />
                                    </p>
                                    <p>
                                        <strong> Invoice</strong> <br />
                                        Invoice No: [invoice_id]  <br />
                                        Date: [date] <br />
                                        Payment: [payment] <br />
                                        Total: [total] [currency]<br />
                                        [notify_cash]
                                    </p>
                                    <p>Sincerely,<br />[blogname]</p>',
			'ban_mail_template'                         => '<p>Hello [display_name],</p><p>You have been banned from [blogname] for reason:</p><p>[reason]</p><p>Your ban will be expired on [ban_expired]</p><p>Please contact our staff for more information</p><p>Sincerely,<br />[blogname]</p>',
			'employer_report_mail_template'             => '<p>Hello [display_name],</p><p>Project [title] you’ve worked on has a new report.&nbsp;</p><p>You can review the project in : [link]</p><p>Sincerely,</p>',
			'employer_close_mail_template'              => "<p>Hi [display_name], </p>
                                                <p>User [employer] has closed the [title] project. Please review and provide admin with detailed information and materials involved in this project.</p>
                                                <p>[link]</p>
                                                <p>If you have any questions, don't hesitate to contact me.</p>
                                                <p>Regards, <br>[blogname]</p>",
			'freelancer_report_mail_template'           => '<p>Hello [display_name],</p>
                                                <p>Your project has a report from [reporter].</p>
                                                <p>You can review the project in : [link]</p>
                                                <p>Sincerely, </br>
                                                [blogname]</p>',
			'freelancer_quit_mail_template'             => "<p>Hi [display_name], </p>
                                                <p>Freelancer [freelancer] has discontinued the [title] project. Please review and provide admin with detailed information and materials involved in this project.</p>
                                                <p>[link]</p>
                                                <p>If you have any questions, don't hesitate to contact me.</p>
                                                <p>Regards, <br>[blogname],</p>",
			'admin_report_mail_template'                => '<p>Hi [display_name], </p>
                                            <p>User [employer] has closed the [title] project on your site. Please review and arbitrate the dispute:</p>
                                            <p>[link]</p>
                                            <p>Regards, <br>[blogname]</p>',
			'admin_report_freelancer_mail_template'     => "<p>Hi [display_name], </p>
                                                        <p>Freelancer [freelancer] has discontinued the [title] project. Please review and arbitrate the dispute:</p>
                                                        <p>[link]</p>
                                                        <p>Regards, <br>[blogname]</p>",
			'fre_refund_mail_template'                  => '<p>Hello [display_name],</p>
                                            <p>The disputing project [title] has been proceed by admin. </p>
                                            <p>The payment will be transferred back to project’s owner</p>
                                            <p>You can review the project in : [link]</p>
                                            <p>Sincerely, </br>
                                            [blogname]</p>',
			'fre_execute_mail_template'                 => '<p>Hello [display_name],
                                            <p>The disputing project [title] has been proceed by admin. </p>
                                            <p>The payment will be transfer to the freelancer</p>
                                            <p>You can review the project in : [link]</p>
                                            <p>Sincerely, </br>
                                            [blogname]</p>',
			'fre_execute_to_employer_mail_template'     => '<p>Hi,</p>
                                                        <p>Your project [title] is completed and the payment has been transferred to freelancer [freelancer].</p>
                                                        <p>You can review the project in:</p>
                                                        <p>[link]</p>
                                                        <p>Regards, <br>[blogname]</p>',
			'fre_execute_to_freelancer_mail_template'   => '<p>Hi,</p>
                                                        <p>Your working project [title] is completed and the payment has been successfully transferred to you. Kindly check your account or your available balance.</p>
                                                        <p>Also, you should review the project and rate for employer [employer] in:</p>
                                                        <p>[link]</p>
                                                        <p>Regards, <br>[blogname]</p>',
			'fre_notify_employer_mail_template'         => '<p>Hi, </p>
                                                        <p>Your project [link] is completed. The payment for this project has been successfully transferred to the freelancer [freelancer].</p>
                                                        <p>Regards, <br>[blogname]</p>',
			'fre_notify_freelancer_mail_template'       => '<p>Hi, </p>
                                                            <p>Your working project [title] is completed and the payment has been successfully transferred to you. Kindly check your account or your available balance.</p>
                                                            <p>Also, you should review the project and rate for Employer [employer] in:</p>
                                                            <p>[link]</p>
                                                            <p>Regards, <br>[blogname]</p>',
			'review_for_employer_mail_template'         => '<p>Hi, </p>
                                                    <p>Freelancer [freelance] has reviewed and rated for you on the project [link].</p>
                                                    <p>Besides, you can visit the project detail tab in your profile page for detailed information.</p>
                                                    <p>[link_profile]</p>
                                                    <p>Regards, <br>[blogname]</p>',
			'bid_cancel_mail_template'                  => "<p>Hello [display_name],</p>
                                    <p>The Freelancer is canceled a bid on the project : [link].</p>
                                    <p>Sincerely,</p>
                                    <p>[blogname]</p>",
			'new_project_mail_template'                 => "<p>Hi there,</p>
                                            <p>There is a new job for you today. Hurry apply for this project [project_link] and get everything started.</p>
                                            <p>Hope you have a highly effective Day</p>",
			'approved_payment_mail_template'            => "<p>Hi [display_name],</p>
                                                <p>Congratulations! Your payment has been approved by the admin. Your [package_name] package is available to be used.<br>
                                                    Here are the details of your transaction:</p>
                                                <p>
                                                    <strong>Customer info</strong>:<br>
                                                    Name: [display_name] <br>
                                                    Email: [user_email]<br>
                                                </p>
                                                <p>
                                                    <strong>Invoice</strong> <br>
                                                    Invoice No: [invoice_id] <br>
                                                    Date: [date]. <br>
                                                    Payment: [payment] <br>
                                                    Total: [total] [currency]<br>
                                                </p>
                                                <p>Regards, <br>
                                                [blogname]</p>",
			'ae_receipt_project_mail'                   => '<p>Dear [display_name],</p>
                                        <p>
                                            Thank you for your payment.<br />
                                            Here are the details of your transaction:<br />
                                            <strong>Detail</strong>:<br />
                                                - Purchase the [package_name] package. This package contains [number_of_posts] project posts.<br />
                                                - Submit a project [link].
                                        </p>
                                        <p>
                                            <strong> Customer info</strong>:<br />
                                            Name: [display_name] <br />
                                            Email: [user_email]. <br />
                                        </p>
                                        <p>
                                            <strong> Invoice</strong> <br />
                                            Invoice No: [invoice_id]  <br />
                                            Date: [date] <br />
                                            Payment: [payment] <br />
                                            Total: [total] [currency]<br />
                                            [notify_cash]
                                        </p>
                                        <p>Sincerely,<br />[blogname]</p>',
			'ae_receipt_bid_mail'                       => '<p>Dear [display_name],</p>
                                    <p>
                                        Thank you for your payment.<br />
                                        Here are the details of your transaction:<br />
                                        <strong>Detail</strong>: Purchase the [package_name] package. This package contains [number_of_bids] project bids.
                                    </p>
                                    <p>
                                        <strong> Customer info</strong>:<br />
                                        Name: [display_name] <br />
                                        Email: [user_email]. <br />
                                    </p>
                                    <p>
                                        <strong> Invoice</strong> <br />
                                        Invoice No: [invoice_id]  <br />
                                        Date: [date] <br />
                                        Payment: [payment] <br />
                                        Total: [total] [currency]<br />
                                        [notify_cash]
                                    </p>
                                    <p>Sincerely,<br />[blogname]</p>',
			'ae_resubmitted_project_mail'               => '<p>Hi,</p>
                                            <p>Employer [display_name] has been re-submitted his previous rejected project [link]. Please review and approve this project.</p>
                                            <p>Regards,<br>[blogname]</p>',
			'fre_notify_employer_when_employer_win'     => '<p>Hi [display_name], </p>
                                                        <p>After reviewing all reasons and information provided by both parties, I decided you are the winner in the dispute occurring on the [link] project.</p>
                                                        <p>Your payment will be refunded, kindly check your either account or available balance.</p>
                                                        <p>Thanks for your continuous support and trust in us.</p>
                                                        <p>Regards, <br>[blogname]</p>',
			'fre_notify_freelancer_when_employer_win'   => "<p>Hi [display_name],</p>
                                                        <p>After reviewing all reasons and and information provided by both parties, I'm sorry to announce that employer [employer] is the winner in the dispute occurring on the [link] project.</p>
                                                        <p>The payment is refunded to the employer.</p>
                                                        <p>If you have any questions, don't hesitate to contact me.</p>
                                                        <p>Thanks for your continuous support and trust in us.</p>
                                                        <p>Regards, <br>[blogname]</p>",
			'fre_notify_employer_when_freelancer_win'   => "<p>Hi [display_name], </p>
                                                        <p>After reviewing all reasons and and information provided by both parties, I'm sorry to announce that freelancer [freelancer] is the winner in the dispute occurring on the [link] project.</p>
                                                        <p>The payment is transferred to the freelancer.</p>
                                                        <p>If you have any questions, don't hesitate to contact me.</p>
                                                        <p>Thanks for your continuous support and trust in us.</p>
                                                        <p>Regards, <br>[blogname]</p>",
			'fre_notify_freelancer_when_freelancer_win' => "<p><p>Hi [display_name], </p>
                                                        <p>After reviewing all reasons and and information provided by both parties, I decided you are the winner in the dispute occurring on [link] project.</p>
                                                        <p>Your payment is successfully transferred to you, kindly check your either account or available balance.</p>
                                                        <p>Thanks for your continuous support and trust in us.</p>
                                                        <p>Regards, <br>[blogname]</p>",
			'init'                                      => 1
		) );
	}

	function update_first_time() {
		update_option( 'de_first_time_install', 1 );
		update_option( 'revslider-valid-notice', 'false' );
	}

	/**
	 * FrE setup wizard html template
	 *
	 * @param string $html
	 *
	 * @return string $html
	 * @since 1.6.2
	 * @package void
	 * @category void
	 * @author Tambh
	 */
	public function fre_setup_wizard_template( $html ) {
		ob_start();
		?>
        <div class="et-main-content" id="overview_settings">
            <div class="et-main-right">
                <div class="et-main-main clearfix inner-content" id="wizard-sample">

                    <div class="title font-quicksand" style="padding-top:0;">
                        <h3><?php _e( 'SAMPLE DATA', ET_DOMAIN ) ?></h3>

                        <div class="desc small"><?php _e( 'The sample data include some items from the list below: profile, project, etc.', ET_DOMAIN ) ?></div>

                        <div class="btn-language padding-top10 f-left-all"
                             style="padding-bottom:15px;height:65px;margin:0;">
							<?php
							$sample_data_op = get_option( 'option_sample_data' );
							if ( ! $sample_data_op ) {
								echo '<button class="primary-button" id="install_sample_data">' . __( "Instalar datos de ejemplo", ET_DOMAIN ) . '</button>';
							} else {
								echo '<button class="primary-button" id="delete_sample_data">' . __( "Eliminar datos de ejemplo", ET_DOMAIN ) . '</button>';
							}
							?>
                        </div>
                    </div>

                    <div class="desc" style="padding-top:0px;">
                        <div class="title font-quicksand sample-title">
                            <a target="_blank" rel="noopener noreferrer"
                               href="<?php echo admin_url( 'edit.php?post_type=project' ); ?>"><?php _e( 'Proyectos', ET_DOMAIN ) ?></a>
                            <span class="description"><?php _e( 'Publicar nuevos proyectos o modificar los existente para se ajusten a tu modelo.', ET_DOMAIN ) ?></span>
                        </div>
                        <div class="title font-quicksand sample-title">
                            <a target="_blank" rel="noopener noreferrer"
                               href="<?php echo admin_url( 'edit-tags.php?taxonomy=project_category&post_type=project' ); ?>"><?php _e( 'Categoría de Proyectos', ET_DOMAIN ) ?></a>
                            <span class="description"><?php _e( 'Agregar nuevas categorías o modificar las existentes de acuerdo a tus Freelancers.', ET_DOMAIN ) ?></span>
                        </div>
                        <div class="title font-quicksand sample-title">
                            <a target="_blank" rel="noopener noreferrer"
                               href="<?php echo admin_url( 'edit-tags.php?taxonomy=country&post_type=project' ); ?>"><?php _e( 'Ubicaciones', ET_DOMAIN ) ?></a>
                            <span class="description"><?php _e( 'Agregar nuevas ubicaciones o modificar las existentes que se ajusten a tu modelo.', ET_DOMAIN ) ?></span>
                        </div>
                        <div class="title font-quicksand sample-title">
                            <a target="_blank" rel="noopener noreferrer"
                               href="<?php echo admin_url( 'edit-tags.php?taxonomy=skill&post_type=project' ); ?>"><?php _e( 'Habilidades', ET_DOMAIN ) ?></a>
                            <span class="description"><?php _e( 'Agregar nuevas habilidades o modificar las existentes que se ajusten a tu modelo', ET_DOMAIN ) ?></span>
                        </div>
                        <div class="title font-quicksand sample-title">
                            <a target="_blank" rel="noopener noreferrer"
                               href="<?php echo admin_url( 'edit.php?post_type=fre_profile' ); ?>"><?php _e( 'Perfiles', ET_DOMAIN ) ?></a>
                            <span class="description"><?php _e( 'Agregar nuevos perfiles o modificar los existentes que se ajusten a tu perfil.', ET_DOMAIN ) ?></span>
                        </div>
                        <div class="title font-quicksand sample-title">
                            <a target="_blank" rel="noopener noreferrer"
                               href="<?php echo admin_url( 'edit.php?post_type=page' ); ?>"><?php _e( 'Páginas', ET_DOMAIN ) ?></a>
                            <span class=><?php _e( 'Agregar páginas adicionales o modifica las existentes como son "Sobre nosotros", "Terminos de uso", and "Blog".', ET_DOMAIN ) ?></span>
                        </div>
                        <div class="title font-quicksand sample-title">
                            <a target="_blank" rel="noopener noreferrer"
                               href="<?php echo admin_url( 'edit.php' ); ?>"><?php _e( 'Publicaciones', ET_DOMAIN ) ?></a> <span
                                    class="description"><?php _e( 'Las publicaciones de ejemplo son agregadas en los datos de ejemplo, puedes editar o modificarlos aquí.', ET_DOMAIN ) ?></span>
                        </div>
                    </div>
                </div>

                <div class="et-main-main clearfix inner-content <?php if ( ! $sample_data_op ) {
					echo 'hide';
				} ?>" id="overview-listplaces">

                    <div class="title font-quicksand" style="padding-bottom:60px;">
                        <h3><?php _e( 'MÁS CONFIGURACIÓN', ET_DOMAIN ) ?></h3>
                        <div class="desc small"><?php _e( 'Mejora tu sitio personalizando estas funciones', ET_DOMAIN ) ?></div>
                    </div>

                    <div style="clear:both;"></div>

                    <div class="title font-quicksand  sample-title">
                        <a target="_blank" rel="noopener noreferrer"
                           href="admin.php?page=et-settings"><?php _e( 'configuración general', ET_DOMAIN ) ?></a> <span
                                class="description"><?php _e( 'Modifica la información de tu interfaz, enlaces sociales, scripts de analiticas, or agrega nuevos idiomas.', ET_DOMAIN ) ?></span>
                    </div>

                    <div class="title font-quicksand sample-title">
                        <a target="_blank" rel="noopener noreferrer"
                           href="edit.php?post_type=page"><?php _e( 'Página princial', ET_DOMAIN ) ?></a> <span
                                class="description"><?php _e( 'Reorganiza el contenido y elementos en la página principal como se ajuste a tus necesidades.', ET_DOMAIN ) ?></span>
                    </div>

                    <div class="title font-quicksand sample-title">
                        <a target="_blank" rel="noopener noreferrer"
                           href="nav-menus.php"><?php _e( 'Menú', ET_DOMAIN ) ?></a> <span
                                class="description"><?php _e( 'Edita todos los menú disponibles en la página', ET_DOMAIN ) ?></span>
                    </div>

                    <div class="title font-quicksand sample-title">
                        <a href="widgets.php" target="_blank"
                           rel="noopener noreferrer"><?php _e( 'Barras y Widgets', ET_DOMAIN ) ?></a> <span
                                class="description"><?php _e( 'Agrega o elimina widgets o modifica los existentes.', ET_DOMAIN ) ?></span>
                    </div>

                </div>
            </div>
        </div>
        <style type="text/css">
            .hide {
                display: none;
            }

            .et-main-left .title, .et-main-main .title {
                text-transform: none;
            }

            .et-main-main {
                margin-left: 0;
            }

            .title.font-quicksand h3 {
                margin-bottom: 0;
                margin-top: 0;
            }

            .desc.small, span.description {
                font-family: Arial, sans-serif !important;
                font-weight: 400;
                font-size: 16px !important;
                color: #9d9d9d;
                font-style: normal;
                margin-top: 10px;
            }

            span.description {
                margin-left: 30px;
            }

            .sample-title {
                color: #427bab !important;
                padding-left: 20px !important;
                font-size: 18px !important;
            }

            .title.font-quicksand {
                padding-top: 15px;
            }

            a.primary-button {
                right: 50px;
                position: absolute;
                text-decoration: none;
                color: #ff9b78;
            }

            .et-main-main .title {
                padding-left: 20px;
            }

            .sample-title a {
                text-decoration: none;
            }
        </style>
		<?php
		$html = ob_get_clean();

		return $html;
	}

	/**
	 * FrE notification after setup theme
	 *
	 * @param string $noti
	 *
	 * @return string $noti
	 * @since 1.6.2
	 * @package void
	 * @category void
	 * @author Tambh
	 */
	public function fre_notice_after_installing_theme( $noti ) {
		$noti = sprintf( __( "Haz instalado Sancocho Dominicana te recomendamos seguir  <a href='%s'>el instalador</a> Para configurar tu sitio <a href='%s'>Cerrar este mensaje</a>", ET_DOMAIN ), admin_url( 'admin.php?page=et-wizard' ), add_query_arg( 'close_notices', '1' ) );

		return $noti;
	}

	/**
	 * Set static page after insert sample data
	 *
	 * @param void
	 *
	 * @return void
	 * @since void
	 * @package void
	 * @category void
	 * @author Tambh
	 */
	public function fre_after_insert_sample_data() {
		if ( $homeid = url_to_postid( et_get_page_link( 'home-new' ) ) ) {
			update_option( 'page_on_front', $homeid );
		}
		if ( $blogid = url_to_postid( et_get_page_link( 'blog' ) ) ) {
			update_option( 'page_for_posts ', $blogid );
		}
		update_option( 'show_on_front', 'page' );
		update_option( 'avatar_default', 'gravatar_default' );
		$ae_option = AE_Options::get_instance();
		$ae_option->update_option( 'social_user_role', array( 'freelancer', 'employer' ) );
		et_change_user_role();
	}

	/**
	 * update admin setup
	 */
	function admin_setup() {
		// disable admin bar for all users except admin
		if ( ! current_user_can( 'administrator' ) && ! is_admin() ) {
			show_admin_bar( false );
		}

		$sections = array();

		/**
		 * general settings section
		 */
		$sections['general-settings'] = array(
			'args'   => array(
				'title' => __( "General", ET_DOMAIN ),
				'id'    => 'general-settings',
				'icon'  => 'y',
				'class' => ''
			),
			'groups' => array(
				array(
					'args' => array(
						'title' => __( "Tïtulo de la página", ET_DOMAIN ),
						'id'    => 'site-name',
						'class' => '',
						'desc'  => __( "Entrar título de la página.", ET_DOMAIN )
					),

					'fields' => array(
						array(
							'id'    => 'blogname',
							'type'  => 'text',
							'title' => __( "Título de la página", ET_DOMAIN ),
							'name'  => 'blogname',
							'class' => 'option-item bg-grey-input'
						)
					)
				),
				array(
					'args' => array(
						'title' => __( "Descripción de la página", ET_DOMAIN ),
						'id'    => 'site-description',
						'class' => '',
						'desc'  => __( "Introducir descripción de la página", ET_DOMAIN )
					),

					'fields' => array(
						array(
							'id'    => 'blogdescription',
							'type'  => 'text',
							'title' => __( "Título de la página", ET_DOMAIN ),
							'name'  => 'blogdescription',
							'class' => 'option-item bg-grey-input '
						)
					)
				),
				array(
					'args' => array(
						'title' => __( "Copyright", ET_DOMAIN ),
						'id'    => 'site-copyright',
						'class' => '',
						'desc'  => __( "Esta información de Copyright aparecerá en el pie de la página.", ET_DOMAIN )
					),

					'fields' => array(
						array(
							'id'    => 'copyright',
							'type'  => 'text',
							'title' => __( "Copyright", ET_DOMAIN ),
							'name'  => 'copyright',
							'class' => 'option-item bg-grey-input '
						)
					)
				),
				array(
					'args' => array(
						'title' => __( "Script de Google Analytics", ET_DOMAIN ),
						'id'    => 'site-analytics',
						'class' => '',
						'desc'  => __( "Google Analytics es un servicio de Google que te permite recibir mejores estadísticas de tu sitio.", ET_DOMAIN )
					),

					'fields' => array(
						array(
							'id'    => 'opt-ace-editor-js',
							'type'  => 'textarea',
							'title' => __( "Script de Google Analytics", ET_DOMAIN ),
							'name'  => 'google_analytics',
							'class' => 'option-item bg-grey-input '
						)
					)
				),
				array(
					'args' => array(
						'title' => __( "Confirmación de Email ", ET_DOMAIN ),
						'id'    => 'user-confirm',
						'class' => '',
						'desc'  => __( "Habilitar esto obligará a los usuarios a configurar su Email luego de registrarse.", ET_DOMAIN )
					),

					'fields' => array(
						array(
							'id'    => 'user_confirm',
							'type'  => 'switch',
							'title' => __( "Confirmaciónd e E-mail", ET_DOMAIN ),
							'name'  => 'user_confirm',
							'class' => ''
						)
					)
				),
				array(
					'args' => array(
						'title' => __( "Notificación a Administradores de nuevos registros", ET_DOMAIN ),
						'id'    => 'sendmail-admin',
						'class' => '',
						'desc'  => __( "Notificar a los administradores vía E-mail sobre nuevos registros realizados.", ET_DOMAIN )
					),

					'fields' => array(
						array(
							'id'    => 'sendmail_admin',
							'type'  => 'switch',
							'title' => __( "Notificación a Administradores de nuevos registros", ET_DOMAIN ),
							'name'  => 'sendmail_admin',
							'class' => ''
						)
					)
				),
				// @version: 1.8.3
				// @des: disable from this version.
				// array(
				// 	'args' => array(
				// 		'title' => __( "Log into admin panel", ET_DOMAIN ),
				// 		'id'    => 'login_init',
				// 		'class' => '',
				// 		'desc'  => __( "Prevent direct login to admin page.", ET_DOMAIN )
				// 	),

				// 	'fields' => array(
				// 		array(
				// 			'id'    => 'login-init',
				// 			'type'  => 'switch',
				// 			'label' => __( "Enable this option will prevent directly login to admin page.", ET_DOMAIN ),
				// 			'name'  => 'login_init',
				// 			'class' => 'option-item bg-grey-input '
				// 		)
				// 	)
				// ),
				array(
					'args'   => array(
						'title' => __( "Enlaces Sociales", ET_DOMAIN ),
						'id'    => 'Social-Links',
						'class' => 'Social-Links',
						'desc'  => __( "Los enlaces sociales se muestran en el pie de la página.", ET_DOMAIN ),

						// 'name' => 'currency'

					),
					'fields' => array()
				),

				array(
					'args' => array(
						'title' => __( "URL de Twitter", ET_DOMAIN ),
						'id'    => 'site-twitter',
						'class' => 'payment-gateway',

						//'desc' => __("Your twitter link .", ET_DOMAIN)

					),

					'fields' => array(
						array(
							'id'    => 'site-twitter',
							'type'  => 'text',
							'title' => __( "URL de Twitter", ET_DOMAIN ),
							'name'  => 'site_twitter',
							'class' => 'option-item bg-grey-input '
						)
					)
				),
				array(
					'args' => array(
						'title' => __( "URL de Facebook", ET_DOMAIN ),
						'id'    => 'site-facebook',
						'class' => 'payment-gateway',

						//'desc' => __(".", ET_DOMAIN)

					),

					'fields' => array(
						array(
							'id'    => 'site-facebook',
							'type'  => 'text',
							'title' => __( "Copyright", ET_DOMAIN ),
							'name'  => 'site_facebook',
							'class' => 'option-item bg-grey-input '
						)
					)
				),
				array(
					'args' => array(
						'title' => __( "Google Plus URL", ET_DOMAIN ),
						'id'    => 'site-google',
						'class' => 'payment-gateway',

						// 'desc' => __("This copyright information will appear in the footer.", ET_DOMAIN)

					),

					'fields' => array(
						array(
							'id'    => 'site-google',
							'type'  => 'text',
							'title' => __( "Google Plus URL", ET_DOMAIN ),
							'name'  => 'site_google',
							'class' => 'option-item bg-grey-input '
						)
					)
				),
				array(
					'args' => array(
						'title' => __( "URL Linkedin", ET_DOMAIN ),
						'id'    => 'site-linkedin',
						'class' => 'payment-gateway',

						// 'desc' => __("This copyright information will appear in the footer.", ET_DOMAIN)

					),

					'fields' => array(
						array(
							'id'    => 'site-linkedin',
							'type'  => 'text',
							'title' => __( "URL Linkedin", ET_DOMAIN ),
							'name'  => 'site_linkedin',
							'class' => 'option-item bg-grey-input '
						)
					)

				),



				array(
					'args' => array(
						'title' => __( "Desabilitar la creación de páginas automáticas", ET_DOMAIN ),
						'id'    => 'auto_create_page',
						'class' => '',
						'desc'  => __( "Desahabilitar la creación de págias automáticas", ET_DOMAIN )
					),

					'fields' => array(
						array(
							'id'    => 'login-init',
							'type'  => 'switch',
							'label' => __( "Habilitar esta opción eliminará la creación de páginas automáticas.", ET_DOMAIN ),
							'name'  => 'auto_create_page',
							'class' => 'option-item bg-grey-input '
						)
					)
				)
			)
		);

		/**
		 * branding section
		 */
		/*
        $sections['branding'] = array(

            'args' => array(
                'title' => __("Branding", ET_DOMAIN) ,
                'id' => 'branding-settings',
                'icon' => 'b',
                'class' => ''
            ) ,

            'groups' => array(
                array(
                    'args' => array(
                        'title' => __("Site logo ", ET_DOMAIN) ,
                        'id' => 'site-logo-black',
                        'class' => '',
                        'name' => '',
                        'desc' => __("Your logo should be in PNG, GIF or JPG format, within 150x50px and less than 1500Kb.", ET_DOMAIN)
                    ) ,

                    'fields' => array(
                        array(
                            'id' => 'opt-ace-editor-js',
                            'type' => 'image',
                            'title' => __("Site Logo", ET_DOMAIN) ,
                            'name' => 'site_logo',
                            'class' => '',
                            'size' => array(
                                '143',
                                '29'
                            )
                        )
                    )
                ) ,
                array(
                    'args' => array(
                        'title' => __("Site logo in front page", ET_DOMAIN) ,
                        'id' => 'site-logo-while',
                        'class' => '',
                        'name' => '',
                        'desc' => __("Your logo should be in PNG, GIF or JPG format, within 150x50px and less than 1500Kb.", ET_DOMAIN)
                    ) ,

                    'fields' => array(
                        array(
                            'id' => 'opt-ace-editor-js',
                            'type' => 'image',
                            'title' => __("Site Logo while", ET_DOMAIN) ,
                            'name' => 'site_logo_white',
                            'class' => '',
                            'size' => array(
                                '143',
                                '29'
                            )
                        )
                    )
                ) ,
                array(
                    'args' => array(
                        'title' => __("Mobile logo", ET_DOMAIN) ,
                        'id' => 'mobile-logo',
                        'class' => '',
                        'name' => '',
                        'desc' => __("Your logo should be in PNG, GIF or JPG format, within 150x50px and less than 1500Kb.", ET_DOMAIN)
                    ) ,

                    'fields' => array(
                        array(
                            'id' => 'opt-ace-editor-js',
                            'type' => 'image',
                            'title' => __("Mobile Logo", ET_DOMAIN) ,
                            'name' => 'mobile_logo',
                            'class' => '',
                            'size' => array(
                                '150',
                                '50'
                            )
                        )
                    )
                ) ,

                array(
                    'args' => array(
                        'title' => __("Mobile Icon", ET_DOMAIN) ,
                        'id' => 'mobile-icon',
                        'class' => '',
                        'name' => '',
                        'desc' => __("This icon will be used as a launcher icon for iPhone and Android smartphones and also as the website favicon. The image dimensions should be 57x57px.", ET_DOMAIN)
                    ) ,

                    'fields' => array(
                        array(
                            'id' => 'opt-ace-editor-js',
                            'type' => 'image',
                            'title' => __("Mobile Icon", ET_DOMAIN) ,
                            'name' => 'mobile_icon',
                            'class' => '',
                            'size' => array(
                                '57',
                                '57'
                            )
                        )
                    )
                ) ,
                array(
                    'args' => array(
                        'title' => __("User default logo & avtar", ET_DOMAIN) ,
                        'id' => 'default-logo',
                        'class' => '',
                        'name' => '',
                        'desc' => __("Your logo should be in PNG, GIF or JPG format, within 150x50px and less than 1500Kb.", ET_DOMAIN)
                    ) ,

                    'fields' => array(
                        array(
                            'id' => 'opt-ace-editor-js',
                            'type' => 'image',
                            'title' => __("User default logo & avtar", ET_DOMAIN) ,
                            'name' => 'default_avatar',
                            'class' => '',
                            'size' => array(
                                '150',
                                '150'
                            )
                        )
                    )
                )
            )
        );
        */
		$sections['content'] = array(
			'args'   => array(
				'title' => __( "Contenido", ET_DOMAIN ),
				'id'    => 'content-settings',
				'icon'  => 'l',
				'class' => ''
			),
			//fre_share_role
			'groups' => array(
				// array(
				//     'args' => array(
				//         'title' => __("Sharing Role Capabilities", ET_DOMAIN) ,
				//         'id' => 'fre-share-role',
				//         'class' => 'fre-share-role',
				//         'desc' => __("Enabling this will make employer and freelancer have the same capabilities.", ET_DOMAIN) ,
				//     ) ,
				//     'fields' => array(
				//         array(
				//             'id' => 'fre_share_role',
				//             'type' => 'switch',
				//             'title' => __("Shared Roles", ET_DOMAIN) ,
				//             'name' => 'fre_share_role',
				//             'class' => 'option-item bg-grey-input '
				//         )
				//     )
				// ) ,
				// array(
				//     'args' => array(
				//         'title' => __("Currency", ET_DOMAIN) ,
				//         'id' => 'content-payment-currency',
				//         'class' => 'content-list-package',
				//         'desc' => __("Enter currency code and sign ....", ET_DOMAIN) ,
				//         'name' => 'content_currency'
				//     ) ,
				//     'fields' => array(
				//         array(
				//             'id' => 'content-currency-code',
				//             'type' => 'text',
				//             'title' => __("Code", ET_DOMAIN) ,
				//             'name' => 'code',
				//             'placeholder' => __("Code", ET_DOMAIN) ,
				//             'class' => 'option-item bg-grey-input '
				//         ) ,
				//         array(
				//             'id' => 'content-currency-code',
				//             'type' => 'text',
				//             'title' => __("Sign", ET_DOMAIN) ,
				//             'name' => 'icon',
				//             'placeholder' => __("Sign", ET_DOMAIN) ,
				//             'class' => 'option-item bg-grey-input '
				//         ) ,
				//         array(
				//             'id' => 'currency-align',
				//             'type' => 'switch',
				//             'title' => __("Align", ET_DOMAIN) ,
				//             'name' => 'align',
				//             'class' => 'option-item bg-grey-input ',
				//             'label_1' => __("Left", ET_DOMAIN) ,
				//             'label_2' => __("Right", ET_DOMAIN) ,
				//         )
				//     )
				// ) ,

				array(
					'args'   => array(
						'title' => __( "Límite de presupuesto", ET_DOMAIN ),
						'id'    => 'pending-post',
						'class' => 'pending-post',
						'desc'  => __( "Configura el límite de 'Budget' en el filtro de la página de 'Projects'.", ET_DOMAIN ),
					),
					'fields' => array(
						array(
							'id'          => 'fre-slide-max-budget',
							'type'        => 'text',
							'title'       => __( "Deslizar máximo de presupuesto", ET_DOMAIN ),
							'name'        => 'fre_slide_max_budget',
							'placeholder' => __( "Deslzar máximo de presupuesto", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input '
						)
					)
				),
				array(
					'args'   => array(
						'title' => __( "Limite de presupuesto para Freelancers", ET_DOMAIN ),
						'id'    => 'pending-post-free',
						'class' => 'pending-post',
						'desc'  => __( "Configurar el límite de 'Freelancer budget'en el filtro de la página de 'Profile'.", ET_DOMAIN ),
					),
					'fields' => array(
						array(
							'id'          => 'fre-slide-max-budget',
							'type'        => 'text',
							'title'       => __( "Deslizar el máximo de presupuesto", ET_DOMAIN ),
							'name'        => 'fre_slide_max_budget_freelancer',
							'placeholder' => __( "Deslizar el máximo de presupuesto", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input '
						)
					)
				),
				array(
					'args'   => array(
						'title' => __( "Publicaciones Pendientes", ET_DOMAIN ),
						'id'    => 'pending-post',
						'class' => 'pending-post',
						'desc'  => __( "Habilitar esta opción pondrá las nuevas publicaciones como pendientes hasta que sean revisadas manualmente.", ET_DOMAIN ),
					),
					'fields' => array(
						array(
							'id'    => 'use_pending',
							'type'  => 'switch',
							'title' => __( "Alinear", ET_DOMAIN ),
							'name'  => 'use_pending',
							'class' => 'option-item bg-grey-input '
						)
					)
				),
				array(
					'args'   => array(
						'title' => __( "Ocultar información de Propuesta", ET_DOMAIN ),
						'id'    => 'hide_bid_info',
						'class' => 'hide_bid_info',
						'desc'  => __( "Habilitar esto permitirá que solo los administradores y empleador puedan ver los detalles del proyecto en la página.", ET_DOMAIN ),
					),
					'fields' => array(
						array(
							'id'    => 'hide_bid_info',
							'type'  => 'switch',
							'title' => __( "Alinear", ET_DOMAIN ),
							'name'  => 'hide_bid_info',
							'class' => 'option-item bg-grey-input ',
							'class'   => 'option-item bg-grey-input ',
							'label_1' => __( "Si", ET_DOMAIN ),
							'label_2' => __( "No", ET_DOMAIN ),
						)
					)
				),

				array(
					'args'   => array(
						'title' => __( "Categorías Máximas", ET_DOMAIN ),
						'id'    => 'max-categories',
						'class' => 'max-categories',
						'desc'  => __( "Colocar un máximo de categorías que los Freelancers  pueden colocar en sus perfiles y que los empleadores pueden colocar cuándo publican un proyecto.", ET_DOMAIN )
					),
					'fields' => array(
						array(
							'id'    => 'max_cat',
							'type'  => 'text',
							'title' => __( "Categorías máximas", ET_DOMAIN ),
							'name'  => 'max_cat',
							'class' => 'option-item bg-grey-input '
						)
					)
				),
				array(
					'args'   => array(
						'title' => __( "Habilidades Máximas", ET_DOMAIN ),
						'id'    => 'max-skill',
						'class' => 'max-skill',
						'desc'  => __( "Colocar un máximo de Habilidades que los Freelancers  pueden colocar en sus perfiles y que los empleadores pueden colocar cuándo publican un proyecto.", ET_DOMAIN )
					),
					'fields' => array(
						array(
							'id'    => 'fre-max-skill',
							'type'  => 'text',
							'name'  => 'fre_max_skill',
							'class' => 'option-item bg-grey-input '
						)
					)
				),
				array(
					'args' => array(
						'title' => __( "Orden en Categoría de Proyectos", ET_DOMAIN ),
						'id'    => 'unit_measurement',
						'class' => '',
						'desc'  => __( "Ordenar lista de categorías por:", ET_DOMAIN )
					),

					'fields' => array(
						array(
							'id'          => 'order-project-category',
							'type'        => 'select',
							'data'        => array(
								'name'  => __( "Nombre", ET_DOMAIN ),
								'slug'  => __( "Slug", ET_DOMAIN ),
								'id'    => __( "ID", ET_DOMAIN ),
								'count' => __( "Conteo", ET_DOMAIN )
							),
							'title'       => __( "Orden en Categoría de Proyectos", ET_DOMAIN ),
							'name'        => 'project_category_order',
							'class'       => 'option-item bg-grey-input ',
							'placeholder' => __( "Orden en Categoría de Proyectos", ET_DOMAIN )
						)
					)
				),
				array(
					'args' => array(
						'title' => __( "Orden por tipo de proyecto", ET_DOMAIN ),
						'id'    => 'unit_measurement',
						'class' => '',
						'desc'  => __( "Ordenar listas por:", ET_DOMAIN )
					),

					'fields' => array(
						array(
							'id'          => 'order-project-type',
							'type'        => 'select',
							'data'        => array(
								'name'  => __( "Nombre", ET_DOMAIN ),
								'slug'  => __( "Slug", ET_DOMAIN ),
								'id'    => __( "ID", ET_DOMAIN ),
								'count' => __( "Conteo", ET_DOMAIN )
							),
							'title'       => __( "Orden por tipo de proyecto", ET_DOMAIN ),
							'name'        => 'project_type_order',
							'class'       => 'option-item bg-grey-input ',
							'placeholder' => __( "Orden por tipo de proyecto", ET_DOMAIN )
						)
					)
				),
				/**
				 * hidden "Disable ocmment" option
				 * @since: 1.8.4
				 * @author : danng
				 */
				// array(
				// 	'args' => array(
				// 		'title' => __( "Disable Comment", ET_DOMAIN ),
				// 		'id'    => 'disable-project-comment',
				// 		'class' => '',
				// 		'desc'  => __( "Disable comment on project page.", ET_DOMAIN )
				// 	),

				// 	'fields' => array(
				// 		array(
				// 			'id'      => 'disable_project_comment',
				// 			'type'    => 'switch',
				// 			'title'   => __( "Align", ET_DOMAIN ),
				// 			'name'    => 'disable_project_comment',

				// 			// 'label' => __("Code", ET_DOMAIN),
				// 			'class'   => 'option-item bg-grey-input ',
				// 			'label_1' => __( "Yes", ET_DOMAIN ),
				// 			'label_2' => __( "No", ET_DOMAIN ),
				// 		)
				// 	)
				// ),
				array(
					'args' => array(
						'title' => __( "Invitado a Propuesta", ET_DOMAIN ),
						'id'    => 'invited-to-bid',
						'class' => '',
						'desc'  => __( "Si habilita esta opción, los freelancers tienen que ser invitados antes de hacer propuestas.", ET_DOMAIN )
					),

					'fields' => array(
						array(
							'id'      => 'invited_to_bid',
							'type'    => 'switch',
							'title'   => __( "Invitado a propuesta", ET_DOMAIN ),
							'name'    => 'invited_to_bid',

							// 'label' => __("Code", ET_DOMAIN),
							'class'   => 'option-item bg-grey-input ',
							'label_1' => __( "Si", ET_DOMAIN ),
							'label_2' => __( "No", ET_DOMAIN ),
						)
					)
				)
			)
		);

		$sections['freelancer'] = array(
			'args'   => array(
				'title' => __( "Freelancer", ET_DOMAIN ),
				'id'    => 'freelancer-settings',
				'icon'  => 'U',
				'class' => ''
			),
			//fre_share_role
			'groups' => array(
				array(
					'args'   => array(
						'title' => __( "Pagar por propuesta", ET_DOMAIN ),
						'id'    => 'pay-to-bid',
						'class' => 'pay-to-bid',
						'desc'  => __( "Habilitar esto obligará el freelancer a pagar antes de hacer una propuesta.", ET_DOMAIN ),
					),
					'fields' => array(
						array(
							'id'    => 'pay_to_bid',
							'type'  => 'switch',
							'title' => __( "Pagar para propuesta", ET_DOMAIN ),
							'name'  => 'pay_to_bid',
							'class' => 'option-item bg-grey-input '
						)
					)
				),
				/**
				 * package plan list
				 */
				array(
					'type' => 'list',
					'args' => array(
						'title'        => __( "Planes de Propuesta", ET_DOMAIN ),
						'id'           => 'list-package',
						'class'        => 'list-package',
						'desc'         => '',
						'name'         => 'bid_plan',
						'custom_field' => 'bid_plan'
					),

					'fields' => array(
						'form'        => '/admin-template/bid-plan-form.php',
						'form_js'     => '/admin-template/bid-plan-form-js.php',
						'js_template' => '/admin-template/bid-plan-js-item.php',
						'template'    => '/admin-template/bid-plan-item.php'
					)
				),
			)
		);

		/**
		 * slug settings
		 */
		$sections['url_slug'] = array(
			'args'   => array(
				'title' => __( "Slug de URL", ET_DOMAIN ),
				'id'    => 'Url-Slug',
				'icon'  => 'i',
				'class' => ''
			),
			'groups' => array(
				array(
					'args'   => array(
						'title' => __( "Proyecto", ET_DOMAIN ),
						'id'    => 'project-slug',
						'class' => 'list-package',
						'desc'  => __( "Introduzca el slug para la página única de cada proyecto", ET_DOMAIN ),
					),
					'fields' => array(
						array(
							'id'          => 'fre_project_slug',
							'type'        => 'text',
							'title'       => __( "Slug para página única de proyecto", ET_DOMAIN ),
							'name'        => 'fre_project_slug',
							'placeholder' => __( "Slug para página única de proyecto", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input ',
							'default'     => 'project'
						)
					)
				),
				array(
					'args'   => array(
						'title' => __( "Publicación de proyecto", ET_DOMAIN ),
						'id'    => 'project-archive_slug',
						'class' => 'list-package',
						'desc'  => __( "Introduzca el slug la página de listado de proyectos", ET_DOMAIN ),
					),
					'fields' => array(
						array(
							'id'          => 'fre_project_archive',
							'type'        => 'text',
							'title'       => __( "Slug para página de listado de proyectos", ET_DOMAIN ),
							'name'        => 'fre_project_archive',
							'placeholder' => __( "Slug para listado de proyectos", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input ',
							'default'     => 'projects'
						)
					)
				),

				array(
					'args'   => array(
						'title' => __( "Categoría de proyectos", ET_DOMAIN ),
						'id'    => 'Project-Category',
						'class' => 'list-package',
						'desc'  => __( "Entre el Slug para las categorías de proyectos", ET_DOMAIN ),
					),
					'fields' => array(
						array(
							'id'          => 'project_category_slug',
							'type'        => 'text',
							'title'       => __( "Slug para página de categorías de proyectos", ET_DOMAIN ),
							'name'        => 'project_category_slug',
							'placeholder' => __( "Slug para página de categorías de proyectos", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input ',
							'default'     => 'project_category',
						)
					)
				),

				array(
					'args'   => array(
						'title' => __( "Tipo de Proyecto", ET_DOMAIN ),
						'id'    => 'Project-Type',
						'class' => 'list-package',
						'desc'  => __( "Introduzca el Slug para la página del tipo de proyecto", ET_DOMAIN ),
					),
					'fields' => array(
						array(
							'id'          => 'project_type_slug',
							'type'        => 'text',
							'title'       => __( "Slug para página de tipo de proyecto", ET_DOMAIN ),
							'name'        => 'project_type_slug',
							'placeholder' => __( "Slug para página de tipo de proyecto", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input ',
							'default'     => 'project_type'
						)
					)
				),

				array(
					'args'   => array(
						'title' => __( "Perfiles", ET_DOMAIN ),
						'id'    => 'Profile-slug',
						'class' => 'list-package',
						'desc'  => __( "Slug para páginas de perfiles", ET_DOMAIN ),
					),
					'fields' => array(
						array(
							'id'          => 'fre_profile_slug',
							'type'        => 'text',
							'title'       => __( "Slug para perfil de usuario", ET_DOMAIN ),
							'name'        => 'author_base',
							'placeholder' => __( "Slug para perfil de usuario", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input ',
							'default'     => 'author'
						)
					)
				),
				array(
					'args'   => array(
						'title' => __( "Lista de perfiles", ET_DOMAIN ),
						'id'    => 'profiles-archive_slug',
						'class' => 'list-package',
						'desc'  => __( "Introduzca el Slug para lista de perfiles", ET_DOMAIN ),
					),
					'fields' => array(
						array(
							'id'          => 'fre_profile_archive',
							'type'        => 'text',
							'title'       => __( "Slug para página de lista de perfiles", ET_DOMAIN ),
							'name'        => 'fre_profile_archive',
							'placeholder' => __( "Slug para página de lista de perfiles", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input ',
							'default'     => 'profiles'
						)
					)
				),

				array(
					'args'   => array(
						'title' => __( "País", ET_DOMAIN ),
						'id'    => 'profile-Country',
						'class' => 'list-package',
						'desc'  => __( "Introduzca el Slug para la página de países", ET_DOMAIN ),
					),
					'fields' => array(
						array(
							'id'          => 'country_slug',
							'type'        => 'text',
							'title'       => __( "Etiqueta de Slug para países", ET_DOMAIN ),
							'name'        => 'country_slug',
							'placeholder' => __( "Etiqueta de Slug para países", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input ',
							'default'     => 'country'
						)
					)
				),

				array(
					'args'   => array(
						'title' => __( "Habilidades", ET_DOMAIN ),
						'id'    => 'profile-Skill',
						'class' => 'list-package',
						'desc'  => __( "Introduzca el Slug para la página de habilidades", ET_DOMAIN ),
					),
					'fields' => array(
						array(
							'id'          => 'skill_slug',
							'type'        => 'text',
							'title'       => __( "Etiqueta para página de Habilidades", ET_DOMAIN ),
							'name'        => 'skill_slug',
							'placeholder' => __( "Etiqueta para página de Habilidades", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input ',
							'default'     => 'skill'
						)
					)
				)
			)
		);

		/**
		 * video background settings section
		 */
		/*
		 * disable from version 1.8.3 by danng.

		$sections['header_video'] = array(

			'args' => array(
				'title' => __( "Header Video", ET_DOMAIN ),
				'id'    => 'header-settings',
				'icon'  => 'V',
				'class' => ''
			),

			'groups' => array(

				array(
					'args' => array(
						'title' => __( "Video Background Url", ET_DOMAIN ),
						'id'    => 'header-slider-settings',
						'class' => '',
						'desc'  => __( "Enter your video background url in page-home.php template (.mp4)", ET_DOMAIN )
					),

					'fields' => array(
						array(
							'id'          => 'header-video',
							'type'        => 'text',
							'title'       => __( "header video url", ET_DOMAIN ),
							'name'        => 'header_video',
							'class'       => 'option-item bg-grey-input ',
							'placeholder' => __( 'Enter your header video url', ET_DOMAIN )
						)
					)
				),
				array(
					'args' => array(
						'title' => __( "Video Background Via Youtube ID", ET_DOMAIN ),
						'id'    => 'header-youtube_id',
						'class' => '',
						'desc'  => __( "Enter youtube ID for background video instead of video url", ET_DOMAIN )
					),

					'fields' => array(
						array(
							'id'          => 'youtube_id-video',
							'type'        => 'text',
							'title'       => __( "header video url", ET_DOMAIN ),
							'name'        => 'header_youtube_id',
							'class'       => 'option-item bg-grey-input ',
							'placeholder' => __( 'Enter youtube video ID', ET_DOMAIN )
						)
					)
				),

				array(
					'args' => array(
						'title' => __( "Video Background Fallback", ET_DOMAIN ),
						'id'    => 'header-slider-settings',
						'class' => '',
						'desc'  => __( "Fallback image for video background when browser not support", ET_DOMAIN )
					),

					'fields' => array(
						array(
							'id'          => 'header-video',
							'type'        => 'text',
							'title'       => __( "header video url", ET_DOMAIN ),
							'name'        => 'header_video_fallback',
							'class'       => 'option-item bg-grey-input ',
							'placeholder' => __( 'Enter your header video fallback image url', ET_DOMAIN )
						)
					)
				),

				array(
					'args' => array(
						'title' => __( "Project Demonstration", ET_DOMAIN ),
						'id'    => 'header-slider-settings',
						'class' => '',
						'name'  => 'project_demonstration'

						// 'desc' => __("Enter your header slider setting", ET_DOMAIN)

					),

					'fields' => array(
						array(
							'id'    => 'header-left-text',
							'type'  => 'text',
							'title' => __( "header left text", ET_DOMAIN ),
							'name'  => 'home_page',
							'class' => 'option-item bg-grey-input ',
							'label' => __( 'Project demonstration on header video background viewed by employer.', ET_DOMAIN )
						),
						array(
							'id'    => 'header-right-text',
							'type'  => 'text',
							'title' => __( "header right text", ET_DOMAIN ),
							'name'  => 'list_project',
							'class' => 'option-item bg-grey-input ',
							'label' => __( 'Project demonstration on header video background viewed by freelancer.', ET_DOMAIN )
						)
					)
				),

				array(
					'args' => array(
						'title' => __( "Profile Demonstration", ET_DOMAIN ),
						'id'    => 'header-slider-settings',
						'class' => '',
						'name'  => 'profile_demonstration'

						// 'desc' => __("Enter your header slider setting", ET_DOMAIN)

					),

					'fields' => array(
						array(
							'id'    => 'header-left-text',
							'type'  => 'text',
							'title' => __( "header left text", ET_DOMAIN ),
							'name'  => 'home_page',
							'class' => 'option-item bg-grey-input ',
							'label' => __( 'Profile demonstration on header video background viewed by freelancer.', ET_DOMAIN )
						),
						array(
							'id'    => 'header-right-text',
							'type'  => 'text',
							'title' => __( "header right text", ET_DOMAIN ),
							'name'  => 'list_profile',
							'class' => 'option-item bg-grey-input ',
							'label' => __( 'Profiles demonstration on list profiles page viewed by employer.', ET_DOMAIN )
						)
					)
				),
				array(
					'args'   => array(
						'title' => __( "Loop Header Video", ET_DOMAIN ),
						'id'    => 'header-video-loop-option',
						'class' => '',
						'desc'  => __( " Enabling this will make the video on the header repeated automatically.", ET_DOMAIN )
					),
					'fields' => array(
						array(
							'id'    => 'header-video-loop',
							'type'  => 'switch',
							'title' => __( "Select video loop", ET_DOMAIN ),
							'name'  => 'header_video_loop',
							'class' => 'option-item bg-grey-input '
						)
					)
				)
			)
		);
		*/

		/**
         * google captcha settings section
         * Re-add from 1.8.4
         * @author : danng
         */
        $sections['gg_captcha'] = array(
            'args' => array(
                'title' => __("Captcha", ET_DOMAIN) ,
                'id'    => 'gg-captcha',
                'icon'  => '3',
                'class' => ''
            ),
            'groups' => array(
                array(
                    'args' => array(
                        'title' => __("Google reCaptcha", ET_DOMAIN) ,
                        'id'    => 'google-recaptcha',
                        'class' => '',
                        'desc'  => __("Habilitando esto inhabilitará Spammers de registrarse.<a href='https://www.google.com/recaptcha/admin#list' target='_blank' rel='nofollow'>get key</a>", ET_DOMAIN)
                    ),
                    'fields' => array(
                        array(
                            'id'    => 'gg_captcha',
                            'type'  => 'switch',
                            'title' => __("Google reCaptcha", ET_DOMAIN) ,
                            'name'  => 'gg_captcha',
                            'class' => ''
                        ),
                        array(
                            'id'          => 'gg_site_key',
                            'type'        => 'text',
                            'title'       => __("Llave del sitio", ET_DOMAIN) ,
                            'name'        => 'gg_site_key',
                            'placeholder' => __("clave del sitio del reCaptcha", ET_DOMAIN) ,
                            'class'       => ''
                        ),
                        array(
                            'id'          => 'gg_secret_key',
                            'type'        => 'text',
                            'title'       => __("Clave secreta", ET_DOMAIN) ,
                            'name'        => 'gg_secret_key',
                            'placeholder' => __("Clave secreta del reCaptcha", ET_DOMAIN) ,
                            'class'       => ''
                        )
                    )
                )
            )
        );


		/**
		 * Payment settings
		 */
		$sections['payment_settings'] = array(
			'args' => array(
				'title' => __( "Pagos", ET_DOMAIN ),
				'id'    => 'payment-settings',
				'icon'  => '%',
				'class' => ''
			),

			'groups' => array(
				array(
					'args'   => array(
						'title' => '<span class="dashicons dashicons-warning" style="color:#ff6c00;"></span> ' . __( "Por favor introduzca la moneda que será aceptada como pago en el sitio.", ET_DOMAIN ),
						'id'    => 'warning-description-group',
						'class' => 'no-desc',
						'name'  => ''
					),
					'fields' => array(
						array(
							'id'    => 'warning-description',
							'type'  => 'desc',
							'title' => "",
							'text'  => "",
							'class' => '',
							'name'  => 'warning_description'
						)
					)
				),
				array(
					'args'   => array(
						'title' => __( "Moneda de pago", ET_DOMAIN ),
						'id'    => 'payment-currency',
						'class' => 'list-package',
						'desc'  => __( "Introduzca el código de la moneda y su símbolo.", ET_DOMAIN ),
						'name'  => 'currency'
					),
					'fields' => array(
						array(
							'id'          => 'currency-code',
							'type'        => 'text',
							'title'       => __( "Código", ET_DOMAIN ),
							'name'        => 'code',
							'placeholder' => __( "Código", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input '
						),
						array(
							'id'          => 'currency-sign',
							'type'        => 'text',
							'title'       => __( "símbolo", ET_DOMAIN ),
							'name'        => 'icon',
							'placeholder' => __( "Símbolo", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input '
						),
						array(
							'id'      => 'currency-align',
							'type'    => 'switch',
							'title'   => __( "alinear", ET_DOMAIN ),
							'name'    => 'align',

							// 'label' => __("Code", ET_DOMAIN),
							'class'   => 'option-item bg-grey-input ',
							'label_1' => __( "Izquierda", ET_DOMAIN ),
							'label_2' => __( "Derecha", ET_DOMAIN ),
						),
					)
				),

				array(
					'args'   => array(
						'title' => __( "Formato de números", ET_DOMAIN ),
						'id'    => 'number-format',
						'class' => 'list-package',
						'desc'  => __( "Formato agrupado en los miles", ET_DOMAIN ),
						'name'  => 'number_format'
					),
					'fields' => array(
						array(
							'id'          => 'decimal-point',
							'type'        => 'text',
							'title'       => __( "Punto decimal", ET_DOMAIN ),
							'label'       => __( "Punto decimal", ET_DOMAIN ),
							'name'        => 'dec_point',
							'placeholder' => __( "Punto decimal", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input '
						),
						array(
							'id'          => 'thousand_sep',
							'type'        => 'text',
							'label'       => __( "Separador de miles", ET_DOMAIN ),
							'title'       => __( "Separador de miles", ET_DOMAIN ),
							'name'        => 'thousand_sep',
							'placeholder' => __( "Separador de miles", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input '
						),
						array(
							'id'          => 'et_decimal',
							'type'        => 'text',
							'label'       => __( "Puntos de total de decimales", ET_DOMAIN ),
							'title'       => __( "Puntos de total de decimales", ET_DOMAIN ),
							'name'        => 'et_decimal',
							'placeholder' => __( "Coloca el total de puntos decimales", ET_DOMAIN ),
							'class'       => 'option-item bg-grey-input positive_int',
							'default'     => 2
						),
					)
				),

				array(
					'args'   => array(
						'title' => __( "Permitir publicaciones gratuitas", ET_DOMAIN ),
						'id'    => 'free-to-submit-listing',
						'class' => 'free-to-submit-listing',
						'desc'  => __( "Habilitar esto permitirá a los usuarios publicar gratuitamente.", ET_DOMAIN ),

						// 'name' => 'currency'


					),
					'fields' => array(
						array(
							'id'    => 'disable-plan',
							'type'  => 'switch',
							'title' => __( "Alinear", ET_DOMAIN ),
							'name'  => 'disable_plan',
							'class' => 'option-item bg-grey-input '
						)
					)
				),

				/* payment test mode settings */
				array(
					'args'   => array(
						'title' => __( "Modo de prueba de pagos", ET_DOMAIN ),
						'id'    => 'payment-test-mode',
						'class' => 'payment-test-mode',
						'desc'  => __( "Habilitar esto permitirá un modo de prueba de pagos, donde harás transacciones sin cargar tu cuenta.", ET_DOMAIN ),

						// 'name' => 'currency'


					),
					'fields' => array(
						array(
							'id'    => 'test-mode',
							'type'  => 'switch',
							'title' => __( "Alinear", ET_DOMAIN ),
							'name'  => 'test_mode',
							'class' => 'option-item bg-grey-input '
						)
					)
				),
				// payment test mode

				/* payment gateways settings */
				array(
					'args'   => array(
						'title' => __( "Portales de pago", ET_DOMAIN ),
						'id'    => 'payment-gateways',
						'class' => 'payment-gateways',
						'desc'  => __( "Configura los portales de pago que tus usuarios pueden utilizar.", ET_DOMAIN ),

						// 'name' => 'currency'

					),
					'fields' => array()
				),

				array(
					'args'   => array(
						'title' => __( "Paypal", ET_DOMAIN ),
						'id'    => 'Paypal',
						'class' => 'payment-gateway',
						'desc'  => __( "Habilitar esto permitirá a los usuarios pagar con Paypal.", ET_DOMAIN ),

						'name' => 'paypal'
					),
					'fields' => array(
						array(
							'id'    => 'paypal',
							'type'  => 'switch',
							'title' => __( "Align", ET_DOMAIN ),
							'name'  => 'enable',
							'class' => 'option-item bg-grey-input '
						),
						array(
							'id'          => 'paypal_mode',
							'type'        => 'text',
							'title'       => __( "Alinear", ET_DOMAIN ),
							'name'        => 'api_username',
							'class'       => 'option-item bg-grey-input ',
							'placeholder' => __( 'Introduzca su dirección de Paypal', ET_DOMAIN )
						)
					)
				),

				array(
					'args'   => array(
						'title' => __( "2Checkout", ET_DOMAIN ),
						'id'    => '2Checkout',
						'class' => 'payment-gateway',
						'desc'  => __( "Habilitar esto permitirá a los usuarios pagar vía 2CheckOut.", ET_DOMAIN ),

						'name' => '2checkout'
					),
					'fields' => array(
						array(
							'id'    => '2Checkout_mode',
							'type'  => 'switch',
							'title' => __( "Modo 2Checkout", ET_DOMAIN ),
							'name'  => 'enable',
							'class' => 'option-item bg-grey-input '
						),
						array(
							'id'          => 'sid',
							'type'        => 'text',
							'title'       => __( "Sid", ET_DOMAIN ),
							'name'        => 'sid',
							'class'       => 'option-item bg-grey-input ',
							'placeholder' => __( 'Your 2Checkout Seller ID', ET_DOMAIN )
						),
						array(
							'id'          => 'secret_key',
							'type'        => 'text',
							'title'       => __( "Secret Key", ET_DOMAIN ),
							'name'        => 'secret_key',
							'class'       => 'option-item bg-grey-input ',
							'placeholder' => __( 'Your 2Checkout Secret Key', ET_DOMAIN )
						)
					)
				),
				array(
					'args'   => array(
						'title' => __( "Efectivo", ET_DOMAIN ),
						'id'    => 'Cash',
						'class' => 'payment-gateway',
						'desc'  => __( "Por favor introduzca su información bancaria, donde los usuarios pueden depositar. Ej: Euric Santi, 13078885679XXX,  BanReservas", ET_DOMAIN ),

						'name' => 'cash'
					),
					'fields' => array(
						array(
							'id'    => 'cash_message_enable',
							'type'  => 'switch',
							'title' => __( "Alinear", ET_DOMAIN ),
							'name'  => 'enable',
							'class' => 'option-item bg-grey-input '
						),
						array(
							'id'    => 'cash_message',
							'type'  => 'editor',
							'title' => __( "Alinear", ET_DOMAIN ),
							'name'  => 'cash_message',
							'class' => 'option-item bg-grey-input ',

							// 'placeholder' => __('Enter your PayPal email address', ET_DOMAIN)

						)
					)
				),
				/**
				 * package plan list
				 */
				array(
					'type' => 'list',
					'args' => array(
						'title' => __( "Planes de pago", ET_DOMAIN ),
						'id'    => 'list-package',
						'class' => 'list-package',
						'desc'  => '',
						'name'  => 'pack',
					),

					'fields' => array(
						'form'        => '/admin-template/package-form.php',
						'form_js'     => '/admin-template/package-form-js.php',
						'js_template' => '/admin-template/package-js-item.php',
						'template'    => '/admin-template/package-item.php'
					)
				),

				// limit_free_plan
				// array(
				//     'args' => array(
				//         'title' => __("Limit Free Plan Use", ET_DOMAIN) ,
				//         'id' => 'limit_free_plan',
				//         'class' => 'limit_free_plan',
				//         'desc' => __("Enter the maximum number allowing employers to use your Free plan.", ET_DOMAIN)
				//     ) ,
				//     'fields' => array(
				//         array(
				//             'id' => 'cash_message_enable',
				//             'type' => 'text',
				//             'title' => __("Align", ET_DOMAIN) ,
				//             'name' => 'limit_free_plan',
				//             'class' => 'option-item bg-grey-input '
				//         )
				//     )
				// ) ,
			)
		);

		/**
		 * mail template settings section
		 */
		$sections['mailing'] = array(
			'args' => array(
				'title' => __( "Mailing", ET_DOMAIN ),
				'id'    => 'mail-settings',
				'icon'  => 'M',
				'class' => ''
			),

			'groups' => array(
				array(
					'args'   => array(
						'title' => __( "Autentificación de Plantilla", ET_DOMAIN ),
						'id'    => 'mail-description-group',
						'class' => '',
						'name'  => ''
					),
					'fields' => array(
						array(
							'id'    => 'mail-description',
							'type'  => 'desc',
							'title' => __( "Descripción del correo", ET_DOMAIN ),
							'text'  => __( "Template de correo para proceso de autentificación. Puedes usar los colocadores para plantillas específicas.", ET_DOMAIN ) . '<a class="icon btn-template-help payment" data-icon="?" href="#" title="Ver más detalles"></a>' . '<div class="cont-template-help payment-setting">
                                                    [user_login],[display_name],[user_email] : ' . __( "Detalles del usuario que recibirá el email", ET_DOMAIN ) . '<br />
                                                    [dashboard] : ' . __( "URL de Escritorio para miembros ", ET_DOMAIN ) . '<br />
                                                    [title], [link], [excerpt],[desc], [author] : ' . __( "Titulo del proyecto, detalles, enlaces, autor", ET_DOMAIN ) . ' <br />
                                                    [activate_url] : ' . __( "El enlace de activación es requerido para autorizar los usuarios", ET_DOMAIN ) . ' <br />
                                                    [site_url],[blogname],[admin_email] : ' . __( " Información del sitio, correo del administrador", ET_DOMAIN ) . '
                                                    [project_list] : ' . __( "Lista de proyectos que los empleadores envían a los freelancers", ET_DOMAIN ) . '

                                                </div>',

							'class' => '',
							'name'  => 'mail_description'
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Plantillas de registro de Email", ET_DOMAIN ),
						'id'     => 'register-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar a los usuarios que se han registrado correctamente.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'register_mail_template',
							'type'  => 'editor',
							'title' => __( "Email registrado", ET_DOMAIN ),
							'name'  => 'register_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),

				array(
					'args'   => array(
						'title'  => __( "Plantilla de registro por red social", ET_DOMAIN ),
						'id'     => 'register-mail-social',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar a los usuarios que se han registrado correctamente vía Redes Sociales.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'register_social_mail_template',
							'type'  => 'editor',
							'title' => __( "Email de Registro", ET_DOMAIN ),
							'name'  => 'register_social_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),

				array(
					'args'   => array(
						'title'  => __( "Plantilla de correo de confirmación", ET_DOMAIN ),
						'id'     => 'confirm-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar a los usuarios después que confirman exitosamente su registro.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'confirm_mail_template',
							'type'  => 'editor',
							'title' => __( "Email de Confirmación", ET_DOMAIN ),
							'name'  => 'confirm_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),

				array(
					'args'   => array(
						'title'  => __( "Plantila de Email Confirmado", ET_DOMAIN ),
						'id'     => 'confirmed-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar para notificar que los usuarios han sido confirmados correctamente.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'confirmed_mail_template',
							'type'  => 'editor',
							'title' => __( "Email Confirmadon", ET_DOMAIN ),
							'name'  => 'confirmed_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),

				array(
					'args'   => array(
						'title'  => __( "Plantilla de Email Contraseña olvidada", ET_DOMAIN ),
						'id'     => 'forgotpass-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar a los usuarios cuándo solicitan reiniciar contraseña.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'forgotpass_mail_template',
							'type'  => 'editor',
							'title' => __( "Reiniciar Contraseña", ET_DOMAIN ),
							'name'  => 'forgotpass_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Plantilla de Reinicio de contraseña", ET_DOMAIN ),
						'id'     => 'resetpass-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al usuario para confirmar que su contraseña ha sido cambiada.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'resetpass_mail_template',
							'type'  => 'editor',
							'title' => __( "Email Reinicio de contraseña", ET_DOMAIN ),
							'name'  => 'resetpass_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Plantilla de Suspención", ET_DOMAIN ),
						'id'     => 'ban-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						// 'desc' => __("Enviar a los usuarios para firmar que su cuenta ha sido suspendia", ET_DOMAIN),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'ban_mail_template',
							'type'  => 'editor',
							'title' => __( "Email de Suspención", ET_DOMAIN ),
							'name'  => 'ban_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),

				array(
					'args'   => array(
						'title' => __( "Plantilla de Emails para proyectos", ET_DOMAIN ),
						'id'    => 'mail-description-group',
						'class' => '',
						'name'  => ''
					),
					'fields' => array(
						array(
							'id'    => 'mail-description',
							'type'  => 'desc',
							'title' => __( "Descripción del Email aquí", ET_DOMAIN ),
							'text'  => __( "Templates para eventualidades concernientes a Proyectos. Puedes usar los contenedores para incluir contenido específico", ET_DOMAIN ),
							'class' => '',
							'name'  => 'mail_description'
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Plantilla para revisión de pago", ET_DOMAIN ),
						'id'     => 'new-payment-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al administrador cuándo el portal tiene un nuevo pago.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'new_payment_mail_template',
							'type'  => 'editor',
							'title' => __( "Plantilla de revisión de pago", ET_DOMAIN ),
							'name'  => 'new_payment_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Plantilla para nuevo mensaje", ET_DOMAIN ),
						'id'     => 'new-message-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar a los usuarios cuándo tienen un nuevo mensaje en su espacio de trabajo.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'new_message_mail_template',
							'type'  => 'editor',
							'title' => __( "Bandeja de Entrada", ET_DOMAIN ),
							'name'  => 'new_message_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),

				array(
					'args'   => array(
						'title'  => __( "Plantilla de bandeja de entrada", ET_DOMAIN ),
						'id'     => 'inbox-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar a los usuarios cuándo quieren ser contactados.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'inbox_mail_template',
							'type'  => 'editor',
							'title' => __( "Bandeja de entrada", ET_DOMAIN ),
							'name'  => 'inbox_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),

				array(
					'args'   => array(
						'title'  => __( "Plantilla de correo de invitación", ET_DOMAIN ),
						'id'     => 'invite-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar a los usuarios cuándo alguien desea invitarles a un proyecto.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'invite_mail_template',
							'type'  => 'editor',
							'title' => __( "Correo de invitación", ET_DOMAIN ),
							'name'  => 'invite_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				// array(
				//     'args' => array(
				//         'title' => __("Cash Notification Mail Template", ET_DOMAIN) ,
				//         'id' => 'cash-mail',
				//         'class' => 'payment-gateway',
				//         'name' => '',
				//         'desc' => __("Send to user cash message when he pays by cash.", ET_DOMAIN),
				//         'toggle' => true
				//     ) ,
				//     'fields' => array(
				//         array(
				//             'id' => 'cash_notification_mail',
				//             'type' => 'editor',
				//             'title' => __("Cash Notification Mail", ET_DOMAIN) ,
				//             'name' => 'cash_notification_mail',
				//             'class' => '',
				//             'reset' => 1
				//         )
				//     )
				// ),

				array(
					'args'   => array(
						'title'  => __( "Plantilla de email recibido", ET_DOMAIN ),
						'id'     => 'ae-receipt_mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'toggle' => true,
						'desc'   => __( "Enviar a los usuarios cuándo el pago ha sido completado", ET_DOMAIN )
					),
					'fields' => array(
						array(
							'id'    => 'ae_receipt_mail',
							'type'  => 'editor',
							'title' => __( "Plantilla de correos recibido", ET_DOMAIN ),
							'name'  => 'ae_receipt_mail',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Pago recibido por adquisición de paquete de pago", ET_DOMAIN ),
						'id'     => 'ae-receipt_project_mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'toggle' => true,
						'desc'   => __( "Enviar al empleador cuando adquiere paquete de pago", ET_DOMAIN )
					),
					'fields' => array(
						array(
							'id'    => 'ae_receipt_project_mail',
							'type'  => 'editor',
							'title' => __( "Notificación para recibo de pago", ET_DOMAIN ),
							'name'  => 'ae_receipt_project_mail',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Notificación por recibo de pago en propuesta realizada", ET_DOMAIN ),
						'id'     => 'ae-receipt_bid_mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'toggle' => true,
						'desc'   => __( "Enviar a los usuarios cuando compran un paquete de propuestas", ET_DOMAIN )
					),
					'fields' => array(
						array(
							'id'    => 'ae_receipt_bid_mail',
							'type'  => 'editor',
							'title' => __( "Notificación por recibo de pago en propuestas realizadas", ET_DOMAIN ),
							'name'  => 'ae_receipt_bid_mail',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Pantilla de Publicación", ET_DOMAIN ),
						'id'     => 'publish-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar a los usuarios cuándo sus proyectos han sido publicados.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'publish_mail_template',
							'type'  => 'editor',
							'title' => __( "Email de Publicación", ET_DOMAIN ),
							'name'  => 'publish_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Notificación de Re-revisión de proyectos enviados", ET_DOMAIN ),
						'id'     => 'resubmmit-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al administrador cuándo el empleador solicita una re-revisión de su proyecto.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'ae_resubmitted_project_mail',
							'type'  => 'editor',
							'title' => __( "Re-revisión de proyectos", ET_DOMAIN ),
							'name'  => 'ae_resubmitted_project_mail',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Plantilla de Archivo de proyecto", ET_DOMAIN ),
						'id'     => 'archive-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Notificar a los usuarios cuándo sus proyectos se remueven debido a expiración o decisión administrativa.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'archive_mail_template',
							'type'  => 'editor',
							'title' => __( "Email de Archivo", ET_DOMAIN ),
							'name'  => 'archive_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Plantilla de Correo de rechazo", ET_DOMAIN ),
						'id'     => 'reject-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar a los usuarios para confirmar que su proycto ha sido rechazado.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'reject_mail_template',
							'type'  => 'editor',
							'title' => __( "Correo de rechazo", ET_DOMAIN ),
							'name'  => 'reject_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Plantilla para nueva propuesta", ET_DOMAIN ),
						'id'     => 'bid-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar a los empleadores cuándo los candidatos hacen propuestas en su proyecto.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'bid_mail_template',
							'type'  => 'editor',
							'title' => __( "Email de propuesta", ET_DOMAIN ),
							'name'  => 'bid_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),

				array(
					'args'   => array(
						'title'  => __( "Plantilla de Propuesta aceptada", ET_DOMAIN ),
						'id'     => 'bid_accepted_-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al freelancer cuándo su propuesta ha sido aceptada.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'bid_accepted_template',
							'type'  => 'editor',
							'title' => __( "Propuesta aceptada", ET_DOMAIN ),
							'name'  => 'bid_accepted_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Proyecto aceptado por el empleador", ET_DOMAIN ),
						'id'     => 'bid_accepted_alternative-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar a los freelancer luego que el empleador a entregado el proyecto a otros freelancers.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'bid_accepted_alternative_template',
							'type'  => 'editor',
							'title' => __( "Proyecto aceptado por empleador", ET_DOMAIN ),
							'name'  => 'bid_accepted_alternative_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Proyecto finalizado por empleador", ET_DOMAIN ),
						'id'     => 'complete-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al freelancer cuándo el proyecto es finalizado por el empleador", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'complete_mail_template',
							'type'  => 'editor',
							'title' => __( "Email de completado", ET_DOMAIN ),
							'name'  => 'complete_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Notificar al empleador cuándo el freelancer le aporta un comentario.", ET_DOMAIN ),
						'id'     => 'review-for-employer',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al empleador cuándo el freelancer ofrecer un comentario ", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'review_for_employer_mail_template',
							'type'  => 'editor',
							'title' => __( "Notificar al empleador cuándo el freelancer hace un comentario.", ET_DOMAIN ),
							'name'  => 'review_for_employer_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Plantilla de nuevo correo", ET_DOMAIN ),
						'id'     => 'new-project-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al freelancer cuándo un proyecto que se ajusta a las categorías de su perfil es publicado", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'new_project_mail_template',
							'type'  => 'editor',
							'title' => __( "Email de nuevo proyecto", ET_DOMAIN ),
							'name'  => 'new_project_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Plantillo de Notificación de pago aprobado", ET_DOMAIN ),
						'id'     => 'approved-payment-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al freelancer luego que un administrador requizó y aprobó el pago.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'approved_payment_mail_template',
							'type'  => 'editor',
							'title' => __( "Notificación de aprobación de pago", ET_DOMAIN ),
							'name'  => 'approved_payment_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title' => __( "Plantilla de reporte de proyecto", ET_DOMAIN ),
						'id'    => 'mail-description-group',
						'class' => '',
						'name'  => ''
					),
					'fields' => array(
						array(
							'id'    => 'mail-description',
							'type'  => 'desc',
							'title' => __( "Descripción del correo aquí", ET_DOMAIN ),
							'text'  => __( "Plantilla utilizada para reporte de evnetos. Puedes utilizar los contenedores para contenido específico", ET_DOMAIN ),
							'class' => '',
							'name'  => 'mail_description'
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Proyecto reportado por Empleador", ET_DOMAIN ),
						'id'     => 'employer-report-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al freelancer cuándo el empleador reporta el proyecto.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'employer_report_mail_template',
							'type'  => 'editor',
							'title' => __( "Correo de reporte de empleador", ET_DOMAIN ),
							'name'  => 'employer_report_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				//admin_report_mail_template
				array(
					'args'   => array(
						'title'  => __( "Notificar al administrador cuándo el empleador cierra un proyecto", ET_DOMAIN ),
						'id'     => 'admin-new-report-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Notificar al administador cuándo el empleador cierra su proyecto.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'admin_report_mail_template',
							'type'  => 'editor',
							'title' => __( "Notificar al Administrador cuándo un proyecto es cerrado", ET_DOMAIN ),
							'name'  => 'admin_report_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Notificar al freelancer cuándo el empleador cierra un proyecto.", ET_DOMAIN ),
						'id'     => 'employer-close-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Notificar al admin cuándo el empleador cierra un proycto.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'employer_close_mail_template',
							'type'  => 'editor',
							'title' => __( "Notificar al freelancer cuándo el empleador cerró un proyecto", ET_DOMAIN ),
							'name'  => 'employer_close_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				//admin_report_mail_template
				array(
					'args'   => array(
						'title'  => __( "Notificar al admin cuándo un freelancer descontinúa un proyecto", ET_DOMAIN ),
						'id'     => 'admin-report-freelancer-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Notificar al admin cuándo el freelancer descontinúa el proyecto en que trabaja.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'admin_report_freelancer_mail_template',
							'type'  => 'editor',
							'title' => __( "Notificar al admin cuándo el freelancer descontinúa el proyecto", ET_DOMAIN ),
							'name'  => 'admin_report_freelancer_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Notificar al empleador cuándo el freelancer descontinúa el proyecto", ET_DOMAIN ),
						'id'     => 'freelancer-quit-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al empleador cuándo el freelancer descontinúa el proyecto", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'freelancer_quit_mail_template',
							'type'  => 'editor',
							'title' => __( "Notificar al empleador cuándo el freelancer descontinúa el proyecto", ET_DOMAIN ),
							'name'  => 'freelancer_quit_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Proyecto reportado por freelancer", ET_DOMAIN ),
						'id'     => 'freelancer-report-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al empleador cuándo el freelancer reporta un proyecto.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'freelancer_report_mail_template',
							'type'  => 'editor',
							'title' => __( "Reporte de Freelancer", ET_DOMAIN ),
							'name'  => 'freelancer_report_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Administrador revertió el pago", ET_DOMAIN ),
						'id'     => 'admin-refund-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar a los usuarios cuándo el administrador revierte el pago de depósito al empleador.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'fre_refund_mail_template',
							'type'  => 'editor',
							'title' => __( "Reversión de pago por el administrador", ET_DOMAIN ),
							'name'  => 'fre_refund_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),

				array(
					'args'   => array(
						'title'  => __( "Plantilla de cancelación de propuesta", ET_DOMAIN ),
						'id'     => 'bid-cancel-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al empleador cuándo el freelancer cancela una propuesta", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'bid_cancel_mail_template',
							'type'  => 'editor',
							'title' => __( "Email de Cancelación de propuestas", ET_DOMAIN ),
							'name'  => 'bid_cancel_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title' => __( "Plantilla por pago relacionado", ET_DOMAIN ),
						'id'    => 'mail-description-group',
						'class' => '',
						'name'  => ''
					),
					'fields' => array(
						array(
							'id'    => 'mail-description',
							'type'  => 'desc',
							'title' => __( "Plantilla de relación de pago", ET_DOMAIN ),
							'text'  => __( "Plantilla utilizada para procesamientos de pago. Puedes utilizar los contenedores en este email", ET_DOMAIN ),
							'class' => '',
							'name'  => 'mail_description'
						)
					)
				),
				// array(
				//     'args' => array(
				//         'title' => __("Admin Executed The Payment", ET_DOMAIN) ,
				//         'id' => 'admin-execute-payment-mail',
				//         'class' => 'payment-gateway',
				//         'name' => '',
				//         'desc' => __("Send to user when admin executes the escrow payment and send to the freelancer.", ET_DOMAIN),
				//         'toggle' => true
				//     ) ,
				//     'fields' => array(
				//         array(
				//             'id' => 'fre_execute_mail_template',
				//             'type' => 'editor',
				//             'title' => __("Admin Execute Payment Mail", ET_DOMAIN) ,
				//             'name' => 'fre_execute_mail_template',
				//             'class' => '',
				//             'reset' => 1
				//         )
				//     )
				// ),
				array(
					'args'   => array(
						'title'  => __( "Notificar al empleador cuándo el pago es enviado", ET_DOMAIN ),
						'id'     => 'admin-execute-to-employer-payment-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al empleador cuándo administrador envía dinero al freelancer.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'fre_execute_to_employer_mail_template',
							'type'  => 'editor',
							'title' => __( "Notificar al empleador de que el pago es enviado", ET_DOMAIN ),
							'name'  => 'fre_execute_to_employer_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Notificar al Freelancer de que el pago es enviado", ET_DOMAIN ),
						'id'     => 'admin-execute-to-freelancer-payment-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al Freelancer cuándo el administrador libera el pago hacia ellos.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'fre_execute_to_freelancer_mail_template',
							'type'  => 'editor',
							'title' => __( "Notificar al freelancer cuándo el pago es liberado.", ET_DOMAIN ),
							'name'  => 'fre_execute_to_freelancer_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Notificar al empleador cuándo el pago es enviado - Deshabilitar transferencia manual.", ET_DOMAIN ),
						'id'     => 'notify-employer-payment-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Notificar al empleador cuándo el proyecto es concluido y el pago es enviado al freelancer.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'fre_notify_employer_mail_template',
							'type'  => 'editor',
							'title' => __( "Notificar al empleador cuándo el pago es enviado - Deshabilitar pago manual.", ET_DOMAIN ),
							'name'  => 'fre_notify_employer_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Notificar al freelancer cuándo el pago es enviado- Deshabilitar transferencia manual.", ET_DOMAIN ),
						'id'     => 'notify-freelancer-payment-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Notificar al freelancer cuándo el empleador concluye un proycto, el pago es completado correctamente.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'fre_notify_freelancer_mail_template',
							'type'  => 'editor',
							'title' => __( "Notificar al freelancer cuándo el pago es enviado - Deshabilitar transferencia manual.", ET_DOMAIN ),
							'name'  => 'fre_notify_freelancer_mail_template',
							'class' => '',
							'reset' => 1
						)
					)
				),

				array(
					'args'   => array(
						'title'  => __( "Notificar al empleador cuándo una disputa es resuelta.", ET_DOMAIN ),
						'id'     => 'notify-freelancer-payment-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al empleador cuándo un administrador lo selecciona como ganador.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'fre_notify_employer_when_employer_win',
							'type'  => 'editor',
							'title' => __( "Notificar al Empleador sobre resolución de disputa.", ET_DOMAIN ),
							'name'  => 'fre_notify_employer_when_employer_win',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Notificar al freelancer sobre resolución de disputa", ET_DOMAIN ),
						'id'     => 'notify-freelancer-payment-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al freelancer cuándo un administrador selecciona el empleador como ganador.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'fre_notify_freelancer_when_employer_win',
							'type'  => 'editor',
							'title' => __( "Notificar al Freelancer sobre resolución de disputa.", ET_DOMAIN ),
							'name'  => 'fre_notify_freelancer_when_employer_win',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Notificar al empleador acerca de resolución de disputa.", ET_DOMAIN ),
						'id'     => 'notify-freelancer-payment-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al empleador cuándo un administrador elige al freelancer como ganador.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'fre_notify_employer_when_freelancer_win',
							'type'  => 'editor',
							'title' => __( "Notificar al empleador sobre resolución de disputa.", ET_DOMAIN ),
							'name'  => 'fre_notify_employer_when_freelancer_win',
							'class' => '',
							'reset' => 1
						)
					)
				),
				array(
					'args'   => array(
						'title'  => __( "Notificar al freelancer cuándo gana disputa.", ET_DOMAIN ),
						'id'     => 'notify-freelancer-payment-mail',
						'class'  => 'payment-gateway',
						'name'   => '',
						'desc'   => __( "Enviar al freelancer cuándo un administrador lo selecciona como ganador.", ET_DOMAIN ),
						'toggle' => true
					),
					'fields' => array(
						array(
							'id'    => 'fre_notify_freelancer_when_freelancer_win',
							'type'  => 'editor',
							'title' => __( "NOtificar al Freelancer cuándo lo eligen como ganador.", ET_DOMAIN ),
							'name'  => 'fre_notify_freelancer_when_freelancer_win',
							'class' => '',
							'reset' => 1
						)
					)
				),
			)
		);

		/**
		 * language settings
		 */
		$sections['language'] = array(
			'args' => array(
				'title' => __( "Lenguaje", ET_DOMAIN ),
				'id'    => 'language-settings',
				'icon'  => 'G',
				'class' => ''
			),

			'groups' => array(
				array(
					'args'   => array(
						'title' => __( "Lenguaje del Portal", ET_DOMAIN ),
						'id'    => 'website-language',
						'class' => '',
						'name'  => '',
						'desc'  => __( "Configure el lenguaje que desea usar en su portal.", ET_DOMAIN )
					),
					'fields' => array(
						array(
							'id'    => 'forgotpass_mail_template',
							'type'  => 'language_list',
							'title' => __( "Registrar Correo", ET_DOMAIN ),
							'name'  => 'website_language',
							'class' => ''
						)
					)
				),
				array(
					'args'   => array(
						'title' => __( "Traductor", ET_DOMAIN ),
						'id'    => 'translator',
						'class' => '',
						'name'  => 'translator',
						'desc'  => __( "Traduce un lenguaje", ET_DOMAIN )
					),
					'fields' => array(
						array(
							'id'    => 'translator-field',
							'type'  => 'translator',
							'title' => __( "Registrar correo", ET_DOMAIN ),
							'name'  => 'translate',
							'class' => ''
						)
					)
				)
			)
		);

		/**
		 * license key settings
		 */
		$sections['update'] = array(
			'args' => array(
				'title' => __( "Update", ET_DOMAIN ),
				'id'    => 'update-settings',
				'icon'  => '~',
				'class' => ''
			),

			'groups' => array(
				array(
					'args'   => array(
						'title' => __( "License Key", ET_DOMAIN ),
						'id'    => 'license-key',
						'class' => '',
						'desc'  => ''
					),
					'fields' => array(
						array(
							'id'    => 'et_license_key',
							'type'  => 'text',
							'title' => __( "License Key", ET_DOMAIN ),
							'name'  => 'et_license_key',
							'class' => ''
						)
					)
				)
			)
		);

		$temp    = array();
		$options = AE_Options::get_instance();

		foreach ( $sections as $key => $section ) {
			$temp[] = new AE_section( $section['args'], $section['groups'], $options );
		}

		$pages = array();

		/**
		 * overview container
		 */
		$container = new AE_Overview( array(
			PROFILE,
			PROJECT
		), true );

		//$statics      =   array();
		// $header      =   new AE_Head( array( 'page_title'    => __('Overview', ET_DOMAIN),
		//                                  'menu_title'    => __('OVERVIEW', ET_DOMAIN),
		//                                  'desc'          => __("Overview", ET_DOMAIN) ) );
		$pages['overview'] = array(
			'args'      => array(
				'parent_slug' => 'et-overview',
				'page_title'  => __( 'Overview', ET_DOMAIN ),
				'menu_title'  => __( 'OVERVIEW', ET_DOMAIN ),
				'cap'         => 'administrator',
				'slug'        => 'et-overview',
				'icon'        => 'L',
				'desc'        => sprintf( __( "%s overview", ET_DOMAIN ), $options->blogname )
			),
			'container' => $container,

			// 'header' => $header


		);

		/**
		 * setting view
		 */
		$container         = new AE_Container( array(
			'class' => '',
			'id'    => 'settings'
		), $temp, '' );
		$pages['settings'] = array(
			'args'      => array(
				'parent_slug' => 'et-overview',
				'page_title'  => __( 'Configuración', ET_DOMAIN ),
				'menu_title'  => __( 'CONFIGURACIÓN', ET_DOMAIN ),
				'cap'         => 'administrator',
				'slug'        => 'et-settings',
				'icon'        => 'y',
				'desc'        => __( "Administra Sancocho Dominicana y su apariencia", ET_DOMAIN )
			),
			'container' => $container
		);

		/**
		 * user list view
		 */

		$container        = new AE_UsersContainer( array(
			'filter' => array(
				'moderate'
			)
		) );
		$pages['members'] = array(
			'args'      => array(
				'parent_slug' => 'et-overview',
				'page_title'  => __( 'Miembros', ET_DOMAIN ),
				'menu_title'  => __( 'MIEMBROS', ET_DOMAIN ),
				'cap'         => 'administrator',
				'slug'        => 'et-users',
				'icon'        => 'g',
				'desc'        => __( "Visión general de miembros registrados", ET_DOMAIN )
			),
			'container' => $container
		);

		/**
		 * order list view
		 */
		$orderlist         = new AE_OrderList( array() );
		$pages['payments'] = array(
			'args'      => array(
				'parent_slug' => 'et-overview',
				'page_title'  => __( 'Pagos', ET_DOMAIN ),
				'menu_title'  => __( 'PAGOS', ET_DOMAIN ),
				'cap'         => 'administrator',
				'slug'        => 'et-payments',
				'icon'        => '%',
				'desc'        => __( "Visión general de los pagos", ET_DOMAIN )
			),
			'container' => $orderlist
		);

		/**
		 * setup wizard view
		 */

		$container = new AE_Wizard();
		$pages[]   = array(
			'args'      => array(
				'parent_slug' => 'et-overview',
				'page_title'  => __( 'Asistente de Configuración', ET_DOMAIN ),
				'menu_title'  => __( 'ASIS. DE CONFIGURACIÓN', ET_DOMAIN ),
				'cap'         => 'administrator',
				'slug'        => 'et-wizard',
				'icon'        => 'S',
				'desc'        => __( "Configura y administra el contenido en tu portal", ET_DOMAIN )
			),
			'container' => $container
		);


		/**
		 *  filter pages config params so user can hook to here
		 */
		$pages = apply_filters( 'ae_admin_menu_pages', $pages );

		/**
		 * add menu page
		 */
		$this->admin_menu = new AE_Menu( $pages );

		/**
		 * add sub menu page
		 */
		foreach ( $pages as $key => $page ) {
			new AE_Submenu( $page, $pages );
		}
	}
}
