<div id="fre-payment-sagepay" class="panel-collapse collapse fre-payment-proccess">
    <form class="modal-form" id="sagepay_form" action="#" method="POST" autocomplete="on">
        <div class="fre-input-field">
            <label class="fre-field-title" for=""><?php _e('First Name',ET_DOMAIN);?></label>
            <input  tabindex="20" id="sagepay_firstname" name="sagepay_firstname" type="text" size="20"   class="form-control bg-default-input not_empty required" placeholder="Jonh" />
        </div>
        <div class="fre-input-field">
            <label class="fre-field-title" for=""><?php _e('Last Name',ET_DOMAIN);?></label>
            <input  tabindex="20" id="sagepay_lastname" name="sagepay_lastname" type="text" size="20"  class="form-control bg-default-input not_empty" placeholder="Smith" />
        </div>
        <div class="fre-input-field">
            <label class="fre-field-title" for=""><?php _e('Address',ET_DOMAIN);?></label>
            <input  tabindex="20" id="sagepay_billingadress" name="sagepay_billingadress" type="text" size="20"  class="form-control bg-default-input not_empty" placeholder="" />
        </div>
        <div class="fre-input-field">
            <label class="fre-field-title" for=""><?php _e('City',ET_DOMAIN);?></label>
            <input  tabindex="20" id="sagepay_billingcity" name="sagepay_billingcity" type="text" size="20"  class="form-control bg-default-input not_empty" placeholder="" />
        </div>
        <div class="fre-input-field">
            <label class="fre-field-title" for=""><?php _e('Post/Zip Code',ET_DOMAIN);?></label>
            <input tabindex="20" id="sagepay_postcode" name="sagepay_postcode" type="text" size="20"  class="form-control bg-default-input not_empty" placeholder=""  />
        </div>
        <div class="fre-input-field">
            <label class="fre-field-title" for=""><?php _e('Country',ET_DOMAIN);?></label>
            <select class=" fre-chosen-single" id="sagepay_country" name="sagepay_country">
                <script type="text/javascript" >
                    document.write(getCountryOptionsListHtml("<?php echo htmlentities('GB'); ?>"));
                </script>
            </select>
        </div>
        <div class="fre-input-field">
            <label class="fre-field-title" for=""><?php _e('State Code (U.S only)',ET_DOMAIN);?></label>
            <select class=" fre-chosen-single" aria-describedby="helpBlock" id="sagepay_state">
                <script type="text/javascript" >
                    document.write(getUsStateOptionsListHtml("<?php echo htmlentities(''); ?>"));
                </script>
            </select>  
        </div>
        <div class="fre-proccess-payment-btn">
            <button class="fre-btn" id="button_sagepay" ><?php _e('Make Payment',ET_DOMAIN);?></button>
        </div>
    </form>
</div>