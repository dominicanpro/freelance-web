<?php
/**
 * Template Name: Homepage New
 */

get_header();
global $user_ID;
?>
<!-- Block Banner -->
<div class="fre-background" id="background_banner" style="background-image: url('<?php echo get_theme_mod("background_banner") ? get_theme_mod("background_banner") : get_template_directory_uri()."/img/fre-bg.png";?>');">
	<div class="fre-bg-content">
		<div class="container">
			<h1 id="title_banner"><?php echo get_theme_mod("title_banner") ? get_theme_mod("title_banner") : __("Encuentra el Freelancer perfecto para tu proyecto, o trabaja en algún proyecto", ET_DOMAIN);?></h1>
			<?php if(!is_user_logged_in()){ ?>
				<?php if(!fre_check_register()){ ?>
					<a class="fre-btn primary-bg-color" href="<?php echo get_post_type_archive_link( PROFILE ); ?>"><?php _e('Encontrar Freelancers', ET_DOMAIN);?></a>
					<a class="fre-btn primary-bg-color" href="<?php echo get_post_type_archive_link( PROJECT ); ?>"><?php _e('Encontrar Proyectos', ET_DOMAIN);?></a>
				<?php }else{ ?>
					<a class="fre-btn primary-bg-color" href="<?php echo et_get_page_link('register', array("role"=>'employer')); ?>"><?php _e('Contrar Freelancer', ET_DOMAIN);?></a>
					<a class="fre-btn primary-bg-color" href="<?php echo et_get_page_link('register', array("role"=>'freelancer')); ?>"><?php _e('Aplicar como Freelancer', ET_DOMAIN);?></a>
				<?php } ?>

			<?php }else{ ?>
				<?php if(ae_user_role($user_ID) == FREELANCER){ ?>
					<a class="fre-btn primary-bg-color" href="<?php echo get_post_type_archive_link( PROJECT ); ?>"><?php _e('Encontrar Proyectos', ET_DOMAIN);?></a>
					<a class="fre-btn primary-bg-color" href="<?php echo et_get_page_link('profile'); ?>"><?php _e('Actualizar perfil', ET_DOMAIN);?></a>
				<?php }else{ ?>
					<a class="fre-btn primary-bg-color" href="<?php echo et_get_page_link('submit-project'); ?>"><?php _e('Publicar un proyecto', ET_DOMAIN);?></a>
					<a class="fre-btn primary-bg-color" href="<?php echo get_post_type_archive_link( PROFILE ); ?>"><?php _e('Encontrar Freelancers', ET_DOMAIN);?></a>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
</div>
<!-- Block Banner -->
<!-- Block How Work -->
<div class="fre-how-work">
	<div class="container">
		<h2 id="title_work"><?php echo get_theme_mod("title_work") ? get_theme_mod("title_work") : __('¿Cómo funciona Sancocho?', ET_DOMAIN);?></h2>
		<div class="row">
			<div class="col-lg-3 col-sm-6">
				<div class="fre-work-block">
					<span>
						<img src="<?php echo get_theme_mod('img_work_1') ? get_theme_mod('img_work_1') : get_template_directory_uri().'/img/1.png';?>" id="img_work_1" alt="">
					</span>
					<p id="desc_work_1"><?php echo get_theme_mod("desc_work_1") ? get_theme_mod("desc_work_1") : __('Publica proyectos para que los freelancers sepan lo que necesitas', ET_DOMAIN);?></p>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6">
				<div class="fre-work-block">
					<span>
						<img src="<?php echo get_theme_mod('img_work_2') ? get_theme_mod('img_work_2') : get_template_directory_uri().'/img/2.png';?>" id="img_work_2" alt="">
					</span>
					<p id="desc_work_2"><?php echo get_theme_mod("desc_work_2") ? get_theme_mod("desc_work_2") : __('Examina en los perfiles, mira las puntuaciones y luego elige a quién más se ajuste a tu necesidad.', ET_DOMAIN);?></p>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6">
				<div class="fre-work-block">
					<span>
						<img src="<?php echo get_theme_mod('img_work_3') ? get_theme_mod('img_work_3') : get_template_directory_uri().'/img/3.png';?>" id="img_work_3" alt="">
					</span>
					<p id="desc_work_3"><?php echo get_theme_mod("desc_work_3") ? get_theme_mod("desc_work_3") : __('Utiliza la plataforma de Sancocho para chatear y cargar archivos', ET_DOMAIN);?></p>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6">
				<div class="fre-work-block">
					<span>
						<img src="<?php echo get_theme_mod('img_work_4') ? get_theme_mod('img_work_4') : get_template_directory_uri().'/img/4.png';?>" id="img_work_4" alt="">
					</span>
					<p id="desc_work_4"><?php echo get_theme_mod("desc_work_4") ? get_theme_mod("desc_work_4") : __('Con nuestra protección, el dinero se libera solo cuándo el trabajo está concluido', ET_DOMAIN);?></p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Block How Work -->
<!-- List Profiles -->
<div class="fre-perfect-freelancer">
	<div class="container">
		<h2 id="title_freelance"><?php echo get_theme_mod("title_freelance") ? get_theme_mod("title_freelance") : __('Encuentra Freelancers para tu proyecto', ET_DOMAIN);?></h2>
		<?php get_template_part( 'home-list', 'profiles' );?>
		<div class="fre-perfect-freelancer-more">
			<a class="fre-btn-o primary-color" href="<?php echo get_post_type_archive_link( PROFILE ); ?>"><?php _e('Ver todos los freelancers', ET_DOMAIN);?></a>
		</div>
	</div>
</div>
<!-- List Profiles -->
<!-- List Projects -->
<div class="fre-jobs-online">
	<div class="container">
		<h2 id="title_project"><?php echo get_theme_mod("title_project") ? get_theme_mod("title_project") : __('Busca todos los trabajos en línea', ET_DOMAIN);?></h2>
		<?php get_template_part( 'home-list', 'projects' );?>
	</div>
</div>
<!-- List Projects -->
<!-- List Testimonials -->
<div class="fre-our-stories">
	<div class="container">
		<h2 id="title_story"><?php echo get_theme_mod("title_story") ? get_theme_mod("title_story") : __('Conoce lo que dicen nuestros usuarios', ET_DOMAIN);?></h2>
		<?php get_template_part( 'home-list', 'testimonial' );?>
	</div>
</div>
<!-- List Testimonials -->
<!-- List Pricing Plan -->
<?php
global $disable_plan, $pay_to_bid;
$disable_plan = (int) ae_get_option( 'disable_plan', false );
$pay_to_bid = ae_get_option( 'pay_to_bid', false );

if( ! $disable_plan || $pay_to_bid ){ ?>
	<div class="fre-service">
		<div class="container">
			<h2 id="title_service">
				<?php
					if( ae_user_role($user_ID) == FREELANCER ){
						echo get_theme_mod("title_service_freelancer") ? get_theme_mod("title_service_freelancer") : __('Elige el servicio que necesitas para enviar propuestas', ET_DOMAIN);
					}else{
						echo get_theme_mod("title_service") ? get_theme_mod("title_service") : __('Elige el nivel de servicios para recibir propuestas', ET_DOMAIN);
					}
				?>
			</h2>
			<?php get_template_part( 'home-list', 'pack' );?>
		</div>
	</div>
<?php } ?>
<!-- List Pricing Plan -->
<!-- List Get Started -->
<div class="fre-get-started">
	<div class="container">
		<div class="get-started-content">
			<?php if(!is_user_logged_in()){ ?>
				<h2 id="title_start"><?php echo get_theme_mod("title_start") ? get_theme_mod("title_start") : __('Necesitas contratar un profesional? Únete a Sancocho Dominicana', ET_DOMAIN);?></h2>
				<?php if(fre_check_register()){ ?>
				<a class="fre-btn fre-btn primary-bg-color" href="<?php echo et_get_page_link('register');?>"><?php _e('Iniciar', ET_DOMAIN)?></a>
				<?php } ?>
			<?php }else{ ?>
				<?php if(ae_user_role($user_ID) == FREELANCER){ ?>
					<h2 id="title_start"><?php echo get_theme_mod("title_start_freelancer") ? get_theme_mod("title_start_freelancer") : __("Vamos a encontrar Freelancers para tu proyecto" , ET_DOMAIN);?></h2>
					<a class="fre-btn fre-btn primary-bg-color" href="<?php echo get_post_type_archive_link( PROJECT ); ?>"><?php _e('Encontrar proyectos', ET_DOMAIN)?></a>
				<?php }else{ ?>
					<h2 id="title_start"><?php echo get_theme_mod("title_start_employer") ? get_theme_mod("title_start_employer") : __('El mejor lugar para encontrar profesionales', ET_DOMAIN);?></h2>
					<a class="fre-btn fre-btn primary-bg-color" href="<?php echo et_get_page_link('submit-project'); ?>"><?php _e('Publicar un proyecto', ET_DOMAIN)?></a>
				<?php } ?>
			<?php } ?>

		</div>
	</div>
</div>
<!-- List Get Started -->
<?php get_footer(); ?>
