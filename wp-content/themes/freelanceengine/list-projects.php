<?php
global $wp_query, $ae_post_factory, $post;
$post_object = $ae_post_factory->get( 'project' );
?>
<ul class="fre-project-list project-list-container">
	<?php
	$postdata = array();
	while ( have_posts() ) {
		the_post();
		$convert    = $post_object->convert( $post );
		$postdata[] = $convert;

		if ( $convert->post_status == 'publish' ) {
			get_template_part( 'template/project', 'item' );
		}
	}
	?>
</ul>
<div class="profile-no-result" style="display: none;">
    <div class="profile-content-none">
        <p><?php _e( 'No hay resultados que se ajusten a tu búsqueda', ET_DOMAIN ); ?></p>
        <ul>
            <li><?php _e( 'Intenta artículos más generalizados', ET_DOMAIN ) ?></li>
            <li><?php _e( 'Intenta otro método de búsqueda', ET_DOMAIN ) ?></li>
            <li><?php _e( 'Intenta buscar por palabra clave', ET_DOMAIN ) ?></li>
        </ul>
    </div>
</div>
<?php wp_reset_query(); ?>
<?php
/**
 * render post data for js
 */
echo '<script type="data/json" class="postdata" >' . json_encode( $postdata ) . '</script>';
?>
